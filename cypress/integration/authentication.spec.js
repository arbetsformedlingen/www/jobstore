/// <reference types="Cypress" />

const sessionPassword = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
const sessionUsername = 'test_authentication@hiqtest.se'
const sessionName = 'Test'
const sessionSurname = 'Authentication'

const sessionPassword2 = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
const sessionUsername2 = 'test_authentication2@hiqtest.se'
const sessionName2 = 'Test2'

context('Authentication', () => {
    before(() => {
        cy.logout();
        cy.createUser(sessionName, sessionSurname, sessionUsername, sessionPassword);
    })

    beforeEach(() => {
        cy.logout();
        cy.visit('/backstage');
    })

    after(() => {
        cy.logout();
        cy.fixture('testuser.json', 'json').then((testUser) => {
            cy.login(testUser.username, testUser.password)
        });
        cy.deleteUser(sessionUsername);
        cy.deleteUser(sessionUsername2);
    })
  

    it('Should authenticate with correct user credentials', () => {
        cy.get('[data-cy=login]').click()
        cy.get('#username')
            .type(sessionUsername).should('have.value', sessionUsername)
        cy.get('#password')
            .type(sessionPassword)
        cy.get('#kc-login').click()
        cy.location('pathname').should('eq', '/backstage')
    })

    it('Should be possible to logout', () => {
        cy.login(sessionUsername, sessionPassword)
        cy.visit('/backstage')
        cy.get('[data-cy=logout]').click()
        cy.get('[data-cy=login]').should('exist')
    })

    it('Should not authenticate with incorrect password', () =>{
        cy.get('[data-cy=login]').click()
        cy.get('#username').type(sessionUsername)
        cy.get('#password').type('IncorrectPassword!#¤%&')
        cy.get('#kc-login').click()
        cy.get('.kc-feedback-text').should('have.text', 'Invalid username or password.')
    })

    it('Should not authenticate with non-existing user', () => {     
        cy.get('[data-cy=login]').click()
        cy.get('#password')
          .type(sessionPassword)
        cy.get('#username')
          .type('nonexisting@user.co.uk')
        cy.get('#kc-login').click()
        cy.get('.kc-feedback-text').should('have.text', 'Invalid username or password.')
    })

    it('Should be possible to create a new user', () => {
        cy.get('[data-cy=register]').click();
        cy.get('#firstName').type(sessionUsername2)
        cy.get('#lastName').type('CreateNewUser')
        cy.get('#email').type(sessionUsername2)
        cy.get('#password').type(sessionPassword2)
        cy.get('#password-confirm').type(sessionPassword2)
        cy.get('#kc-form-buttons').click()
        // cy.location('pathname').should('eq', '/backstage') //If verify email is turned off.
        cy.get('.kc-feedback-text').should('have.text', 'You need to verify your email address to activate your account.') //If verify email is turned on.
    });
})