/// <reference types="Cypress" />

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

describe.skip('Create Test Services', () => {

    before(function () {
        cy.fixture('categories.json').as('categories');
        cy.fixture('industries.json').as('industries');
        cy.fixture('testuser.json', 'json').then((testUser) => {
            cy.login(testUser.username, testUser.password)
            cy.visit('/backstage')
        });
    });

    it('Should be possible to create a service', function() {
        const categoryIds = this.categories
        const category = categoryIds['workerCategoryIDs'][randomInt(0, categoryIds['workerCategoryIDs'].length)]
        const industryIDs = this.industries
        const industry = industryIDs['industryIDs'][randomInt(0, industryIDs['industryIDs'].length)]

        cy.get('.dismiss').click()
        cy.fixture('servicesGoogle.json').as('servicesGoogle').then((servicesCollection) => {
            cy.log(servicesCollection)
            var i;
            for (i = 0; i < servicesCollection.length; i++) {
                cy.get('[data-cy=addNewService]').click()
                cy.get('[data-cy=linkToService]').type(servicesCollection[i]['url'])
                cy.get('[data-cy=serviceName]').type(servicesCollection[i]['title'])
                cy.get('[data-cy=serviceAbstract]').type(servicesCollection[i]['description'])
                cy.get('[data-cy=serviceDescription]').type(servicesCollection[i]['description'])
                cy.get('[data-cy=arbetstagare]').parent().click()
                cy.get(category).parent().click('topLeft')
                cy.get(industry).parent().click('left')
                cy.get('[data-cy=addTags]').find('input')
                    .type('jobb_' + String(i) + ' '
                        + 'gig_' + String(i) + ' '
                        + 'gig_' + String(i-1) + ' '
                        + 'gig_' + String(i+1) + ' '
                        + 'headhunter_' + String(i) + ' '
                        + 'headhunter_' + String(i-1) + ' '
                        + 'headhunter_' + String(i+1) + ' '
                        + servicesCollection[i]['name'].replace(/\s/g,'') + ' '
                        + 'test'
                        +'{enter}'
                    )
                cy.get('[data-cy=chooseImage]').click()
                cy.get('[data-cy=linkToServiceImage]').type(servicesCollection[i]['image'])
                cy.get('[data-cy=getImageFromLink]').click()
                cy.wait(2000) //timing issue
                cy.get('[data-cy=uploadImage]')
                    .click()
                // cy.get('[data-cy=publish]').click()
                cy.get('.slider').click()
                cy.get('[data-cy=save]').click()
            }
        });
    })

})