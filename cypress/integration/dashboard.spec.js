/// <reference types="Cypress" />

const runDate = new Date().toISOString();

describe('Dashboard', function () {

  before(function () {
    cy.fixture('testuser.json', 'json').then((testUser) => {
        cy.login(testUser.username, testUser.password)
    });
    cy.fixture('tags.json').as('tags');
    cy.fixture('categories.json').as('categories');
    cy.fixture('industries.json').as('industries');
    cy.visit('/backstage');
  })

  after(function () {
    cy.logout();
  })

  function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  describe('Administrate Services', function () {

    it('Should be possible to create a service from the dashboard', function () {
        const tagCollection = this.tags
        const categoryIds = this.categories
        const category = categoryIds['employerCategoryIDs'][randomInt(0, categoryIds['employerCategoryIDs'].length)]
        const industryIDs = this.industries
        const industry = industryIDs['industryIDs'][randomInt(0, industryIDs['industryIDs'].length)]
        const serviceTitle = runDate + '_Titel'

        cy.visit('/backstage');
        cy.fixture('testuser.json').then((testUser) => {
            cy.get('.bs-welcome').contains('Välkommen ' + testUser.name)
        })
        cy.get('[data-cy=addNewService]').click()

        cy.get('.dismiss').click()

        cy.get('[data-cy=linkToService]').type('https://www.hiq.se')
        cy.get('[data-cy=serviceName]').type(serviceTitle)
        cy.get('[data-cy=serviceAbstract]').type(runDate + ' Sammanfattning av tjänst')
        cy.get('[data-cy=serviceDescription]').type(runDate + ' Beskrivning')
        cy.get('[data-cy=arbetsgivare]').parent().click()
        cy.get(category).parent().click('topLeft')
        cy.get(industry).parent().click('left')
        cy.get('[data-cy=addTags]')
          .find('input')
          .type(runDate + '_TEST, TEST, '
            + tagCollection['animals'][randomInt(0,tagCollection['animals'].length)] + ','
            + tagCollection['computerOccupations'][randomInt(0,tagCollection['computerOccupations'].length)] + ','
            + tagCollection['scientificOccupations'][randomInt(0,tagCollection['scientificOccupations'].length)] + ','
            + tagCollection['healthcareOccupations'][randomInt(0,tagCollection['healthcareOccupations'].length)] + ','
            + tagCollection['artisticOccupations'][randomInt(0,tagCollection['artisticOccupations'].length)] + ','
            + tagCollection['danceOccupations'][randomInt(0,tagCollection['danceOccupations'].length)] + ','
            + tagCollection['tvOccupations'][randomInt(0,tagCollection['tvOccupations'].length)] + ','
            + tagCollection['theatreOccupations'][randomInt(0,tagCollection['theatreOccupations'].length)] + ','
            + tagCollection['corporateTitles'][randomInt(0,tagCollection['corporateTitles'].length)] + ','
            + tagCollection['industrialOccupations'][randomInt(0,tagCollection['industrialOccupations'].length)] + ','
            + tagCollection['lawenforcementOccupations'][randomInt(0,tagCollection['lawenforcementOccupations'].length)] + ' '
            + 'toBeRemoved'
            + '{enter}'
        )
        cy.get('[aria-label=toberemoved]').find('delete-icon').click().then(() => {
          cy.get('[aria-label=toberemoved]').should('not.exist');
        })
        cy.get('[data-cy=chooseImage]').click()
        cy.get('[data-cy=linkToServiceImage]')
          .type('https://www.hiq.se/globalassets/bilder/hiq_bg_bild_some.jpg')
          .type('{enter}')
          //Due to performance issues :<
        cy.get('.move', {timeout: 10000}).should('be.visible')
        cy.get('[data-cy=uploadImage]').scrollIntoView().should('be.visible')
        cy.wait(200)
        cy.get('[data-cy=uploadImage]').click()
        cy.get('.slider').click()
        cy.get('[data-cy=save]').click()
        cy.logout()

        cy.visit('/search')
        cy.get('#targetGrRadioInline1').parent().click('left')
        cy.get('[data-cy=search-field]').type(serviceTitle + '{enter}')
        cy.get('.result-title').contains(serviceTitle)
    })
    
    it.skip('Should be possible to publish services', function () {

    })

    it.skip('Should be possible to unpublish services', function () {

    })

  })
  describe('Administrate Organisation', function () {
    
    it.skip('Should be able to update existing organisation', function () {

    })

  })  
})
    