/// <reference types="Cypress" />

const runDate = new Date().toISOString();
const sessionPassword = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
const sessionUsername = 'test_backstage@hiqtest.se'
const sessionName = 'Test_Backstage'
const sessionSurname = 'Test_Backstage'

describe('Onboarding', () => {

    before( function() {
        cy.logout();
        cy.createUser(sessionName, 'CreateOrganisation', sessionUsername, sessionPassword);
        cy.fixture('categories.json').as('categories');
        cy.fixture('industries.json').as('industries');
    })

    beforeEach(function(){
        cy.logout();
        cy.visit('/backstage');
    })

    afterEach(function(){
    })

    after(function() {
        cy.deleteUser(sessionUsername);
    })

    it.only('Should be able to create a new organisation and service from onboarding', function () {
        const category = this.categories
        const categoryIds = category['workerCategoryIDs']
        const industryIds = this.industries['industryIDs']
        const serviceName = 'Service name ' + runDate
        const organisationName = 'Organisation ' + runDate
        //cy.login(sessionName, sessionPassword) //Login does not work after user creation. Keeps admin logged in.
        cy.visit('/backstage');

        // BAD PRACTICE
        cy.get('[data-cy=login]').click()
        cy.get('#username').type(sessionUsername)
        cy.get('#password').type(sessionPassword)
        cy.get('#kc-login').click()
        cy.location('pathname').should('eq', '/backstage')
        // END BAD PRACTICE

        //Fill in organisation information
        cy.get('.dismiss').click()
        cy.get('[data-cy=addOrganisationName]').type(organisationName)
        cy.get('[data-cy=addOrganisationNumber]').type('1.2.826.0.1.3558696')
        cy.get('[data-cy=chooseImage]').click()
        cy.get('[data-cy=linkToServiceImage]').type('https://is3-ssl.mzstatic.com/image/thumb/Music62/v4/87/c8/f5/87c8f558-141c-5110-a08b-3dd86e5f3c06/source/300x300bb-75.jpg')
        cy.get('[data-cy=getImageFromLink]').click()
        cy.get('[data-cy=uploadImage]').scrollIntoView()
        
        // Due to timing issues :<
        cy.get('.move', {timeout: 10000}).should('be.visible').then(() => {
            cy.get('[data-cy=uploadImage]').scrollIntoView()
            cy.get('[data-cy=uploadImage]').should('be.visible')
            cy.wait(200).then(() => {
                cy.get('[data-cy=uploadImage]').click()
            })
        })

        cy.get('.toast-message').should('not.be.visible')
        cy.get('.toast-message').should('not.have.text', 'Misslyckades med att ladda upp din bild till servern, försök igen senare')
        // cy.get('[data-cy=acceptCodeOfConduct]').click() // .slider for now
        cy.get('.slider').click()
        cy.get('[data-cy=codeOfConductAccepted]').should('be.visible')
        cy.get('[data-cy=organisationNext]').click()
        cy.get('[data-cy=createServiceHeader]').should('be.visible')

        //Fill in service information
        cy.get('[data-cy=arbetstagare]').parent().click('left')

        for(let categoryId of categoryIds){
            cy.get(categoryId).parent().click('topLeft')
        }

        cy.get('[data-cy=url]').type('hiq.se')
        cy.get('[data-cy=getServiceMetaData]').click()
        cy.get('.modal-content').should('be.visible')
            .then(() => {    
                cy.get('.cropper', {timeout: 10000}).should('be.visible')
                    .then(() => {
                        cy.wait(200) // Due to timing issues :<
                        cy.get('[data-cy=uploadImage]').click()
            })
        })
        cy.get('.toast-message').should('not.have.text', 'Misslyckades med att ladda upp din bild till servern, försök igen senare')
        cy.get('[data-cy=name]').type(serviceName)
        for(let industryID of industryIds){
            cy.get(industryID).parent().click('topLeft')
        }
        cy.fixture('tags.json').then((tags) => {
            cy.get('[data-cy=addTags]').find('input').type(tags['computerOccupations'].join('{enter}').split(' ').join(''))
            .type('{enter}')
        })
        cy.get('[data-cy=youtube]').type('https://youtu.be/u1FdwzY6ob0')
        cy.get('[data-cy=socialTwitter]').type('https://twitter.com/arbetsformed')
        cy.get('[data-cy=socialInstagram]').type('https://www.instagram.com/arbetsformedlingen/?hl=sv')
        cy.get('[data-cy=socialLinkedIn]').type('https://www.linkedin.com/company/arbetsformedlingen/?originalSubdomain=se')
        cy.get('[data-cy=createService]').click()
        cy.contains(serviceName)
        cy.contains(organisationName.substring(0, 32))
    })

    it.skip('Should be possible to request to join an organisation upon registration', function(){
        // cy.createUser('Test', 'Testsson', sessionUsername, sessionPassword);
        cy.login(sessionUsername, sessionPassword);
        // Create organisation with api
        cy.visit('/backstage')
        cy.get('[data-cy=addOrganisationName]')
          .type('Test')
        // ...
    })

    it.skip('Should be possible to accept requests to join the organisation', () => {
        cy.fixture('testuser.json', 'json').then((testUser) => {
            cy.login(testUser.username, testUser.password)
            cy.findOrganisationByName('sharp');
        });
        // cy.findOrganisationByName('sharp');
    })
})