// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })




//Login with KeyCloak:
//Source: https://vrockai.github.io/blog/2017/10/28/cypress-keycloak-intregration/
function createUUID() {
  var s = [];
  var hexDigits = '0123456789abcdef';
  for (var i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
  }
  s[14] = '4';
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
  s[8] = s[13] = s[18] = s[23] = '-';
  var uuid = s.join('');
  return uuid;
}

function authenticate(){
  const keyCloakPage = Cypress.env('keyCloakPage');
  const kcRealm = Cypress.env('kcRealm');
  const kcClientID = Cypress.env('kcClientID');
  const registrationPageRequest = {
    url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/registrations`,
    method: 'GET',
    qs: {
      client_id: kcClientID,
      redirect_uri: Cypress.config().baseUrl + '/backstage',
      state: createUUID(),
      nonce: createUUID(),
      response_mode: 'fragment',
      response_type: 'code',
      scope: 'openid'
    }
  };
  return registrationPageRequest
}

//Login with KeyCloak:
//Source: https://vrockai.github.io/blog/2017/10/28/cypress-keycloak-intregration/
Cypress.Commands.add('login', (username, password) => {
  const keyCloakPage = Cypress.env('keyCloakPage');
  const kcRealm = Cypress.env('kcRealm');
  const kcClientID = Cypress.env('kcClientID');
  const loginPageRequest = {
    url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/auth`,
    qs: {
      client_id: kcClientID,
      redirect_uri: Cypress.config().baseUrl,
      state: createUUID(),
      nonce: createUUID(),
      response_mode: 'fragment',
      response_type: 'code',
      scope: 'openid'
    }
  };

  return cy.request(loginPageRequest)
           .then(submitLoginForm)
           .then((response) => {
             cy.log('Submit login form')         
             cy.log(response)
           });

  function submitLoginForm(response) {
    const _el = document.createElement('html');
    _el.innerHTML = response.body;
    // This should be more strict depending on your login page template.
    const loginForm = _el.getElementsByTagName('form');
    const isAlreadyLoggedIn = !loginForm.length;
    if (isAlreadyLoggedIn) {
      cy.log('Already logged in!')
      return;
    }

    cy.log('Keycloak Action')
    cy.log(String(loginForm[0].action))
    
    return cy.request({
      form: true,
      method: 'POST',
      url: loginForm[0].action,
      followRedirect: false,
      body: {
        username: username,
        password: password        
      }
    });
  }
});


Cypress.Commands.add('logout', () => {
  //auth/admin/realms/{realm}/users/{username}/logout
  const keyCloakPage = Cypress.env('keyCloakPage');
  const kcRealm = Cypress.env('kcRealm');
  const kcClientID = Cypress.env('kcClientID');
  const logoutPageRequest = {
    method: 'GET',
    url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/logout`,
    redirect_uri: `${keyCloakPage}/backstage`,
  };
  return cy.request(logoutPageRequest);
})


Cypress.Commands.add('logoutAdmin', (userID) => {
  //POST /admin/realms/{realm}/users/{id}/logout
  const keyCloakPage = Cypress.env('keyCloakPage');
  const kcRealm = Cypress.env('kcRealm');
  const kcClientID = Cypress.env('kcClientID');
  const logoutAdminRequest = {
    method: 'POST',
    url: `${keyCloakPage}/auth/admin/realms/${kcRealm}/users/${userID}/logout`,
    qs: {
      client_id: kcClientID
    }
  }
  return cy.request(logoutAdminRequest);
})


Cypress.Commands.add('getUsers', (searchString) => {
  // GET /admin/realms/{realm}/users
  const keyCloakPage = Cypress.env('keyCloakPage');
  const kcRealm = Cypress.env('kcRealm');
  cy.fixture('testuser.json', 'json').then((testUser) => {
    const getBearerTokenRequest = {
      method: 'POST',
      url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/token`,
      form: true,
      body: {
        grant_type: 'password',
        client_id: 'admin-cli',
        username: testUser.username,
        password: testUser.password
      }
    }
    cy.request(getBearerTokenRequest).then((response) => {
      const getUsersRequest = {
        method: 'GET',
        url: `${keyCloakPage}/auth/admin/realms/${kcRealm}/users`,
        headers: {
          'Authorization':  'Bearer ' + String(response.body['access_token'])
        },
        qs: {
          first: 0,
          max: 20,
          search: searchString
        }
      };
      return cy.request(getUsersRequest).body;
    });
  });
});


Cypress.Commands.add('deleteUser', (userName) => {
  // POST /admin/realms/{realm}/users/{id}
  cy.getUsers(userName).then((listOfUsers) => {
    if (listOfUsers.body.length > 0 && listOfUsers['body'][0]['username'] === userName){
      const userID = listOfUsers['body'][0]['id'];
      const keyCloakPage = Cypress.env('keyCloakPage');
      const kcRealm = Cypress.env('kcRealm');
      cy.fixture('testuser.json', 'json').then((testUser) => {
        const getBearerTokenRequest = {
          method: 'POST',
          url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/token`,
          form: true,
          body: {
            grant_type: 'password',
            client_id: 'admin-cli',
            username: testUser.username,
            password: testUser.password
          }
        }
        cy.request(getBearerTokenRequest).then((response) => {
          const deleteUserRequest = {
            method: 'DELETE',
            url: `${keyCloakPage}/auth/admin/realms/${kcRealm}/users/${userID}`,
            headers: {
              'Authorization':  'Bearer ' + String(response.body['access_token'])
            }
          };
          cy.request(deleteUserRequest);
        });
      }) 
    }
    else {
      cy.log('User ' + String(userName) + ' not found');
    }
  })  
});


Cypress.Commands.add('createUser', (name, surname, email, password) => {
  const keyCloakPage = Cypress.env('keyCloakPage');
  const kcRealm = Cypress.env('kcRealm');
  const kcClientID = Cypress.env('kcClientID');
  const registrationPageRequest = {
    url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/registrations`,
    method: 'GET',
    qs: {
      client_id: kcClientID,
      redirect_uri: Cypress.config().baseUrl + '/backstage',
      state: createUUID(),
      nonce: createUUID(),
      response_mode: 'fragment',
      response_type: 'code',
      scope: 'openid'
    }
  };

  function submitCreateUserForm(response){
    // POST /admin/realms/{realm}/users
    // `${keyCloakPage}/auth/admin/realms/${kcRealm}/login-actions/registration`,
    // `${keyCloakPage}/auth/admin/realms/${kcRealm}/users`,
    const _el = document.createElement('html');
    _el.innerHTML = response.body;
    const registerForm = _el.getElementsByTagName('form');
    const createUserRequest = {
      method: 'POST',
      // url: `${keyCloakPage}/auth/admin/realms/${kcRealm}/users`,
      url: registerForm[0].action,//`${keyCloakPage}/auth/admin/realms/${kcRealm}/users`,
      form: true,
      followRedirect: false,
      body: {
        firstName: name,
        lastName: surname,
        email: email,
        password: password,
        "password-confirm": password,
        emailVerified: true,
        enabled: true
      }
    };
    cy.log(`submitCreateUserForm: ${createUserRequest}`)
    return cy.request(createUserRequest)
  }

  function submitConfirmUser(response){
    //PUT /admin/realms/{realm}/users/{id}
    cy.getUsers(email).then((listOfUsers) => {
      if (listOfUsers.body.length > 0 && listOfUsers['body'][0]['username'] === email){
        const userID = listOfUsers['body'][0]['id'];
        const keyCloakPage = Cypress.env('keyCloakPage');
        const kcRealm = Cypress.env('kcRealm');
        cy.fixture('testuser.json', 'json').then((testUser) => {
          const getBearerTokenRequest = {
            method: 'POST',
            url: `${keyCloakPage}/auth/realms/${kcRealm}/protocol/openid-connect/token`,
            form: true,
            body: {
              grant_type: 'password',
              client_id: 'admin-cli',
              username: testUser.username,
              password: testUser.password
            }
          }
          //curl -v -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $token" --data "{\"enabled\": false}" "http://localhost:8080/auth/admin/realms/[my-realm]/users/[user-id]"
          cy.request(getBearerTokenRequest).then((response) => {
            const confirmUserRequest = {
              method: 'PUT',
              url: `${keyCloakPage}/auth/admin/realms/${kcRealm}/users/${userID}`,
              form: false,
              followRedirect: false,
              headers: {
                'Authorization':  'Bearer ' + String(response.body['access_token'])
              },
              body: {
                emailVerified: true
              }
            };
            return cy.request(confirmUserRequest);
          });
        })
      }
      else{
        cy.log(`User ${email} not found`)
        return
      }
    })
  }


  return cy.request(registrationPageRequest)
           .then(submitCreateUserForm)
           .then(submitConfirmUser);
});



Cypress.Commands.add('typeTab', (shiftKey, ctrlKey) => {
  cy.focused().trigger('keydown', {
      keyCode: 9,
      which: 9,
      shiftKey: shiftKey,
      ctrlKey: ctrlKey
  });
});

Cypress.Commands.add('findOrganisationByName', (organisationName) => {
  const findOrganisationRequest = {
    method: 'POST',
    url: 'http://acc.jtech.se/sp/api/organisation/v1/find/',
    form: true,
    body: {
      name: organisationName
    }
  }
  
  cy.request(authenticate()).then((response) => {
    cy.log(response)
    cy.request(findOrganisationRequest).then((response) => {
      cy.log(reponse)
    })
    
  })
});



