export const environment = {
  production: true, /*false när man kör lokalt, annars true*/

  keycloak: {
    url: '/auth',
    realm: 'jobtechdev',
    clientId: 'job-tech-dev'
  },

  serviceProviderUrl: '/sp/api',
  useKeycloak: true, /*false när man kör lokalt, annars true*/

  webVersion: require('../../package.json').version
};
