import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {KeycloakService} from './app/shared/services/keycloak/keycloak.service';
import 'hammerjs';

if (environment.production) {
  enableProdMode();
}
/* skall vara kommenterad när men publicerar men avkommenterad när man kör lokalt*/
/* platformBrowserDynamic().bootstrapModule(AppModule)
   .catch(err => console.log(err));*/


KeycloakService.init(environment.keycloak, {onLoad: 'check-sso'})
  .then(() => {
    platformBrowserDynamic().bootstrapModule(AppModule)
      .catch(err => console.error('Failed to start application, ', err));

  })
  .catch((e: any) => {
    console.error('Error in bootstrap: ' + JSON.stringify(e));
    throw e;
  });
