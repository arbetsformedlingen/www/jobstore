import {ServiceSummary} from './service-summary';

export class SearchResult {
  resultsOnPage: number;
  totalNoOfResults: number;
  pageNumber: number;
  totalPages: number;
  first: boolean;
  last: boolean;
  result: ServiceSummary[];
  tags: {};
}
