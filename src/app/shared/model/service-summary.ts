import {Category} from './category';

export class ServiceSummary {
  identifier: string;
  imgUrl: string;
  name: string;
  description: string;
  homepage: string;
  categories: string[];
  published: boolean;
  banned: boolean;
}

