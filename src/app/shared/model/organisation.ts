import {User} from './user';
import {ServiceSummary} from './service-summary';

export class Organisation {
  name: string;
  email: string;
  identifier: string;
  description: string;
  number: string;
  logoUrl: string;
  currentUserEditAccess: boolean;
  allowDirectMembership: boolean;
  waiting: User[];
  members: User[];
  services: ServiceSummary[];
}
