export class ExternalActor {
  name: string;
  url: string;
  // Maybe add a 'type' property so we can filter the list for different pages?
}
