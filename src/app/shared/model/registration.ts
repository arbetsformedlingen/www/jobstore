import {Category} from './category';
import {Industry} from './industry';

export class Registration {
  name: string;
  identifier: string;
  description: string;
  public youtube: string;
  public socialTwitter: string;
  public socialInstagram: string;
  public socialLinkedIn: string;
  public targetGroup: string;
  public categories: Category[] = [];
  public industries: Industry[] = [];
  public shortDescription: string;
  tags: string[];
  published: boolean;
  banned: boolean;
  homepage: string;
  logoUrl: string;
  organisation: {
    identifier: string;
    name: string;
  };
}
