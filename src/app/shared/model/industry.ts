
export class Industry {

  identifier: string;
  title: string;
  description: string;
  usedByNoOfServices: number;

  constructor(identifier, title?, description?, usedByNoOfServices?) {
    this.identifier = identifier;
    this.title = title;
    this.description = description;
    this.usedByNoOfServices = usedByNoOfServices;
  }
}
