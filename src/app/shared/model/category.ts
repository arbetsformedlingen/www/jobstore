
export class Category {
  identifier: string;
  name: string;
  description: string;
  targetWorker: boolean;
  targetEmployer: boolean;
  subs: Category[];
}
