import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

import {Registration} from '../model/registration';
import {SearchResult} from '../model/search-result';

@Injectable()
export class ServiceSummaryService {

  private serviceProviderUrl = environment.serviceProviderUrl;
  public isCollapsed = true;

  constructor(private http: HttpClient) {
  }

  getTags() {
    const url = `${environment.serviceProviderUrl}/v1/service/toptentags`;
    console.log(url);
    return this.http.get<string[]>(url);
  }

  get(registrationId: string): Observable<Registration> {
    const url = `${environment.serviceProviderUrl}/v1/jobstore/service/${registrationId}`;
    return this.http.get<Registration>(url);
  }

  search(query: string, category: string, orderBy: string, sortOrder: string, page: number, pageSize: number, reducingTagSearch: boolean): Observable<SearchResult> {

    let url = `${environment.serviceProviderUrl}/v1/jobstore/services?`;

    if (query && query.length > 0) {
      url = url + '&query=' + query;
    }

    if (category !== null) {
      url = url + '&category=' + category;
    }

    if (orderBy !== null) {
      url = url + '&orderby=' + orderBy;
    }

    if (sortOrder !== null) {
      url = url + '&sortorder=' + sortOrder;
    }

    if (page !== null) {
      url = url + '&page=' + page;
    }

    if (pageSize !== null) {
      url = url + '&pageSize=' + pageSize;
    }
    if (reducingTagSearch !== undefined) {
      url = url + '&reducingTagSearch=' + reducingTagSearch;
    }
    return this.http.get<SearchResult>(url);
  }
}
