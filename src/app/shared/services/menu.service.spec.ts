import {inject, TestBed} from '@angular/core/testing';

import {MenuService} from './menu.service';

describe('MenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenuService]
    });
  });

  it('should be created', inject([MenuService], (service: MenuService) => {
    expect(service).toBeTruthy();
  }));

  it('should not be visible', inject([MenuService], (service: MenuService) => {
    expect(service.isVisible()).toBeFalsy();
  }));

  it('should be visible', inject([MenuService], (service: MenuService) => {
    service.toggleMenu();
    expect(service.isVisible()).toBeTruthy();
  }));

  it('should not be visible after hide', inject([MenuService], (service: MenuService) => {
    service.toggleMenu();
    service.hideMenu();
    expect(service.isVisible()).toBeFalsy();
  }));
});
