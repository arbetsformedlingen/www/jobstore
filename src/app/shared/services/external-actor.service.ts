import {Injectable} from '@angular/core';
import {EXTERNAL_ACTORS} from '../model/mock-actors';
import {ExternalActor} from '../model/external-actor';


@Injectable()
export class ExternalActorService {

  loadExternalActors() {
    return EXTERNAL_ACTORS;
  }

}
