import { Injectable } from '@angular/core';
import { KeycloakService } from './keycloak/keycloak.service';
import { Organisation } from '../model/organisation';
import { User } from '../model/user';
import { environment } from '../../../environments/environment';
import { Registration } from '../model/registration';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';


@Injectable()
export class OrganisationService {

  private ws;

  constructor(
    public idp: KeycloakService,
    private http: HttpClient) {
    this.ws = environment.serviceProviderUrl;
  }

  public addOrganisationImage(organisation: Organisation, imageFile: File): Observable<any> {
    console.log('sending image to backend!', imageFile);
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/image`;

    const formData = new FormData();
    formData.append('imageFile', imageFile);

    const options = {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.idp.client().token}` })
    };

    return this.http.post(url, formData, options);
  }

  removeImage(organisation: Organisation) {
    console.log('removing org image ');
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/image`;

    return this.http.delete(url, this.createHeader());
  }

  public getMyOrganisation(): Observable<Organisation> {
    const userId = this.idp.client().tokenParsed.sub;
    const url = `${this.ws}/v1/organisation/member/${userId}`;
    return this.http.get<Organisation>(url, this.createHeader());
  }

  public create(organisation: Organisation): Observable<Organisation> {
    const url = `${this.ws}/v1/organisation`;
    return this.http.post<Organisation>(url, organisation, this.createHeader());
  }

  public update(organisation: Organisation): Observable<Organisation> {
    const url = `${this.ws}/v1/organisation`;
    return this.http.put<Organisation>(url, organisation, this.createHeader());
  }

  public delete(organisation: Organisation) {
    const url = `${this.ws}/v1/organisation/${organisation.identifier}`;
    return this.http.delete(url, this.createHeader());
  }

  public requestMembership(organisation: Organisation): Observable<Organisation> {
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/join-request`;
    const userRequest = {
      name: this.idp.client().tokenParsed['name'],
      keycloakId: this.idp.client().tokenParsed.sub
    };
    console.log('requestMembership: ', organisation, userRequest);
    return this.http.post<Organisation>(url, userRequest, this.createHeader());
  }

  public findNameOrganisations(name): Observable<any> {
    const url = `${this.ws}/v1/organisation/find?name=${name}`;
    return this.http.get<User[]>(url, this.createHeader());
  }

  public isNameAvailable(name): Observable<any> {
    const url = `${this.ws}/v1/organisation/isNameAvailable?name=${name}`;
    return this.http.get<User[]>(url, this.createHeader());
  }


  public findOrganisationMembers(organisation: Organisation, accepted: boolean): Observable<User[]> {
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/members?accepted=${accepted}`;
    return this.http.get<User[]>(url, this.createHeader());
  }

  public acceptOrRejectOrganisationMember(organisation: Organisation, member: User, accepted: boolean): Observable<Organisation> {
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/member-accept/${member.keycloakId}?accepted=${accepted}`;
    return this.http.put<Organisation>(url, '', this.createHeader());
  }

  public removeOrganisationMember(organisation: Organisation, member: User): Observable<Organisation> {
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/member/${member.keycloakId}`;
    return this.http.delete<Organisation>(url, this.createHeader());
  }

  public sendInvite(organisation: Organisation, emails: Array<string>) {
    const url = `${this.ws}/v1/organisation/${organisation.identifier}/invites`;
    return this.http.post<Organisation>(url, emails, this.createHeader());
  }

  public acceptInvite(code: string): Observable<Organisation> {
    const url = `${this.ws}/v1/organisation/invite/accepted`;
    return this.http.post<Organisation>(url, code, this.createHeader());
  }

  private createHeader() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.idp.client().token}`
      })
    };
  }

}
