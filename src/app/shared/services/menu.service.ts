import {Injectable} from '@angular/core';

@Injectable()
export class MenuService {

  extendedMenu = 'closed';
  isActive = false;

  constructor() {
  }

  toggleMenu() {
    this.extendedMenu = this.extendedMenu === 'closed' ? 'open' : 'closed';
    this.isActive  = !this.isActive;
  }

  hideMenu() {
    this.extendedMenu = 'closed';
  }

  isVisible(): boolean {
    return this.extendedMenu === 'open';
  }
}
