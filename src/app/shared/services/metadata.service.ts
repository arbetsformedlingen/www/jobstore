import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Registration} from '../model/registration';
import {shareReplay, tap} from 'rxjs/operators';

const CACHE_SIZE = 1;

@Injectable({
  providedIn: 'root'
})
export class MetadataService {

  private data: Observable<Metadata>;

  public get metadata(): Observable<Metadata> {
    if (this.data) {
      // console.log('Using cache');
      return this.data;
    }
    // console.log('Calling live server');
    return this.data = this.fetch().pipe(
      shareReplay(CACHE_SIZE)
    );
  }

  constructor(private http: HttpClient) { }

  private fetch(): Observable<Metadata> {
    const url = `${environment.serviceProviderUrl}/v1/metadata`;
    return this.http.get<Metadata>(url);
  }
}

export class Metadata {
  imageStoreUrl: string;
  version: string;
  buildTime: string;
  environment: string;
  seoIndexed: boolean;
}
