import {Injectable} from '@angular/core';
import {Observable, interval, BehaviorSubject, of, Subject, EMPTY} from 'rxjs';
import {KeycloakService} from './keycloak/keycloak.service';
import {environment} from '../../../environments/environment';
import {OrganisationService} from './organisation.service';
import {Organisation} from '../model/organisation';
import {Angulartics2GoogleTagManager} from 'angulartics2/gtm';


@Injectable({
  providedIn: 'root',
})
export class UserService {

  public userOrganisation: Subject<Organisation>;
  private isOrganisationLoaded = false;

  constructor(
    private tagManager: Angulartics2GoogleTagManager,
    public idp: KeycloakService,
    private organisationService: OrganisationService) {

    this.userOrganisation = new BehaviorSubject<Organisation>(null);

    if (this.idp.authenticated()) {
      console.log('user is authenticated');
      this.tagManager.setUsername(this.idp.client().subject);
      this.isOrganisationLoaded = false;
      this.reloadOrganisation();
    } else {
      console.log('user is not authenticated');
      this.tagManager.setUsername(null);
    }

    this.userOrganisation.subscribe(org => {
      if (org) {
        console.log('user is part organisation ');
      } else if (this.isOrganisationLoaded) {
        console.log('user is not part of an organisation');
      }
    });

    this.idp.client().onAuthSuccess = () => {
      console.log('onAuthSuccess');
      organisationService.getMyOrganisation().subscribe(org => {
        this.userOrganisation.next(org);
      });
    };
    this.idp.client().onAuthRefreshSuccess = () => {
      console.log('onAuthRefreshSuccess');
    };
    this.idp.client().onAuthRefreshError = () => {
      console.log('onAuthRefreshError');
    };
    this.idp.client().onReady = (authenticated) => {
      console.log('onReady ', authenticated);
    };
    this.idp.client().onTokenExpired = () => {
      console.log('onTokenExpired');
    };
    this.idp.client().onAuthLogout = () => {
      console.log('onAuthLogout');
      this.userOrganisation.next(null);
      this.tagManager.setUsername(null);
    };
    this.idp.client().onAuthError = (error) => {
      console.log('onAuthError', error);
      this.userOrganisation.next(null);
      this.tagManager.setUsername(null);
    };


    interval(30000)
      .subscribe(i => {
        if (this.idp.authenticated()) {
          this.idp.client().updateToken(30)
            .error(() => console.log('Failed to refresh the token, or the session has expired'));
        }
      });
  }

  public hasLoggedIn(): boolean {
    return this.idp.authenticated();
  }

  public updateOrganisation(organisation: Organisation) {
    this.userOrganisation.next(organisation);
  }

  public reloadOrganisation() {
    this.organisationService.getMyOrganisation().subscribe(
      org => {
        // console.log('users organisation was loaded successful');
        this.userOrganisation.next(org);
      }, error => {
        if (error.status === 404) {
          // console.log('confirmed that user is not part of any organisation');
          this.userOrganisation.next(null);
        } else {
          console.log('Organisation error ', error);
        }
      }, () => this.isOrganisationLoaded = true);
  }
}


