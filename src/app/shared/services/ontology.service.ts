
import {of as observableOf,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable()
export class OntologyService {

  constructor(
    private http: HttpClient) { }

  industries(text: string): Observable<any> {
    return observableOf([
      'Administration, ekonomi, juridik',
      'Bygg och anläggning',
      'Chefer och verksamhetsledare',
      'Data/IT',
      'Försäljning, inköp, marknadsföring',
      'Hantverksyrken',
      'Hotell, restaurang, storhushåll',
      'Hälso- och sjukvård',
      'Industriell tillverkning',
      'Installation, drift, underhåll',
      'Kropps- och skönhetsvård',
      'Kultur, media, design',
      'Militärt arbete',
      'Naturbruk',
      'Naturvetenskapligt arbete',
      'Pedagogiskt arbete',
      'Sanering och renhållning',
      'Socialt arbete',
      'Säkerhetsarbete',
      'Tekniskt arbete',
      'Transport']);
  }

  professions(text: string): Observable<any> {
    const url = `http://ontologi.arbetsformedlingen.se/ontology/v1/concept?filter=${text}&type=occupation&limit=10`;
    return this.http
      .get(url).pipe(
      map((data: Array<string>) => data.map((row: any) => row.name)));
  }

  competences(text: string): Observable<any> {
    const url = `http://ontologi.arbetsformedlingen.se/ontology/v1/concept?filter=${text}&type=skill&limit=10`;
    return this.http
      .get(url).pipe(
      map((data: Array<string>) => data.map((row: any) => row.name)));
  }


}
