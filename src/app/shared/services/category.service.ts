import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {KeycloakService} from './keycloak/keycloak.service';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Category} from '../model/category';
import {Registration} from '../model/registration';
import {map, shareReplay} from 'rxjs/operators';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

const CACHE_SIZE = 20;

@Injectable()
export class CategoryService {

  public asynCategories: Observable<any>;

  constructor(private apollo: Apollo) {

    this.asynCategories = this.apollo
      .watchQuery({
        query: gql`
          {
            categories {
              identifier
              name
              description
              targetWorker
              targetEmployer
              subs {
                identifier
                name
                description
                targetWorker
                targetEmployer
                subs {
                  identifier
                  name
                  description
                  targetWorker
                  targetEmployer
                   subs {
                  identifier
                  name
                  description
                  targetWorker
                  targetEmployer
                  }
                }
              }
            }
          }
        `,
      })
      .valueChanges.pipe(
        shareReplay(CACHE_SIZE)
      )
    .pipe(
      map(res => res.data['categories'])
    );
  }
}
