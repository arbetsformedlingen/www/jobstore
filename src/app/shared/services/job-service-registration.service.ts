
import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Registration} from '../model/registration';
import {environment} from '../../../environments/environment';
import {KeycloakService} from './keycloak/keycloak.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ServiceSummary} from '../model/service-summary';
import {Organisation} from '../model/organisation';

@Injectable()
export class JobServiceRegistrationService {

  constructor(
    public idp: KeycloakService,
    private http: HttpClient) {
  }

  get(registrationId: string): Observable<Registration> {
    const url = `${environment.serviceProviderUrl}/v1/service/${registrationId}`;
    return this.http.get<Registration>(url, this.createHeader());
  }

  create(registration: Registration): Observable<any> {
    const url = `${environment.serviceProviderUrl}/v1/service/create`;
    return this.http.post(url, registration, this.createHeader());
  }

  update(registration: Registration): Observable<any> {
    if (registration.identifier == null) {
      return observableThrowError('The registration is not saved ' + registration.identifier);
    }

    const url = `${environment.serviceProviderUrl}/v1/service/update`;
    return this.http.put(url, registration, this.createHeader());
  }

  remove(registrationId): Observable<any> {
    const url = `${environment.serviceProviderUrl}/v1/service/${registrationId}`;

    return this.http.delete(url, this.createHeader());
  }

  publish(registration: ServiceSummary): Observable<any> {
    registration.published = true;
    const url = `${environment.serviceProviderUrl}/v1/service/publish/${registration.identifier}`;

    return this.http.put(url, registration, this.createHeader());
  }

  unpublish(registration: ServiceSummary): Observable<any> {
    registration.published = false;
    const url = `${environment.serviceProviderUrl}/v1/service/unpublish/${registration.identifier}`;

    return this.http.put(url, registration, this.createHeader());
  }

  public addImage(registration: Registration, imageFile: File): Observable<any> {
    console.log('calling backend add service image ', imageFile, registration);
    const url = `${environment.serviceProviderUrl}/v1/service/${registration.organisation.identifier}/${registration.identifier}/image`;

    const formData = new FormData();
    formData.append('imageFile', imageFile);

    const options = {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.idp.client().token}` })
    };

    return this.http.post(url, formData, options);
  }

  removeImage(registration: Registration) {
    console.log('calling backend remove service image ');
    const url = `${environment.serviceProviderUrl}/v1/service/${registration.organisation.identifier}/${registration.identifier}/image`;

    return this.http.delete(url, this.createHeader());
  }

  private createHeader() {
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${this.idp.client().token}`
      })
    };
  }
}
