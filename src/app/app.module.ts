import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {MenuService} from './shared/services/menu.service';
import {ServiceSummaryService} from './shared/services/service-summary.service';
import {ExternalActorService} from './shared/services/external-actor.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {JobServiceRegistrationService} from './shared/services/job-service-registration.service';
import {KeycloakService} from './shared/services/keycloak/keycloak.service';
import {CategoryService} from './shared/services/category.service';
import {ModalService} from './shared/services/modal.service';
import {CookieLawModule} from 'angular2-cookie-law';
import {NgxSpinnerModule} from 'ngx-spinner';
import {GraphQLModule} from './graphql.module';
import {OrganisationService} from './shared/services/organisation.service';
import {UserService} from './shared/services/user.service';
import {Angulartics2Module} from 'angulartics2';
import {MaterialModule} from './material';
import { GoneComponent } from './shared/gone/gone.component';

@NgModule({
  declarations: [
    AppComponent,
    GoneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CookieLawModule,
    NgxSpinnerModule,
    GraphQLModule,
    Angulartics2Module.forRoot(),
    MaterialModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'se'},
    MenuService,
    ServiceSummaryService,
    ExternalActorService,
    JobServiceRegistrationService,
    ExternalActorService,
    KeycloakService,
    CategoryService,
    ModalService,
    UserService,
    OrganisationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
