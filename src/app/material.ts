import {MatButtonModule, MatIconModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {MatMenuModule} from '@angular/material/menu';
import {MatTreeModule} from '@angular/material/tree';

@NgModule({
  imports: [MatButtonModule, MatIconModule],
  exports: [MatButtonModule, MatIconModule],
})
export class MaterialModule { }