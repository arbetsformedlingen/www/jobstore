import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from './about/about.component';
import {HomeComponent} from './home/home.component';
import {MainComponent} from './main.component';
import {ServiceDetailsComponent} from './service-details/service-details.component';
import {SearchComponent} from './search/search.component';
import {AccessibilityComponent} from "./accessibility/accessibility.component";

const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [
      {path: '', component: HomeComponent},
      {path: 'home', redirectTo: '', pathMatch: 'full'},
      {path: 'about', component: AboutComponent},
      {path: 'accessibility', component: AccessibilityComponent},
      {path: 'service/:identifier', component: ServiceDetailsComponent},
      {path: 'service', component: HomeComponent},
      {path: 'search', component: SearchComponent}]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
