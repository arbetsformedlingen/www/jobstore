import { Component, OnInit, ElementRef, ViewChild, AfterContentChecked } from '@angular/core';
import {Meta} from '@angular/platform-browser';
import {MetadataService} from '../shared/services/metadata.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterContentChecked {

  @ViewChild('base') elementView: ElementRef;
  public viewHeight: number;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    this.viewHeight = this.elementView.nativeElement.offsetHeight;
  }

}
