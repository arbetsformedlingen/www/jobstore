import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Apollo, QueryRef} from 'apollo-angular';
import gql from 'graphql-tag';
import {Angulartics2GoogleTagManager} from 'angulartics2/gtm';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService implements OnDestroy {

  private searchType;
  public queryTerm = '';
  private previousQueryTerm = '';
  public targetGroup = 'WORKER';
  public maxNrOfTags = 200;
  public page = 1;
  private pageSize = 50;
  private lastPage = false;
  public totalNumberOfItems = 0;

  public selectedIndustries: string[] = [];
  public selectedCategories: string[] = [];

  public servicesSubject: BehaviorSubject<any[]>;

  private searchQuery: QueryRef<SearchResult, any>;
  public searchResponse: Observable<SearchResult>;

  private categoriesQuery: QueryRef<CategoryCount[], any>;
  public categoriesResponse: Observable<CategoryCount[]>;

  private industriesQuery: QueryRef<IndustryCount[], any>;
  public industriesResponse: Observable<IndustryCount[]>;

  private tagsQuery: QueryRef<TagResult, any>;
  public tagsResponse: Observable<TagCount[]>;

  private searchSub: Subscription;

  public get services(): Observable<any[]> {
    return this.servicesSubject;
  }

  constructor(private graphql: Apollo,
              private tagManager: Angulartics2GoogleTagManager) {
    this.servicesSubject = new BehaviorSubject([]);
  }

  ngOnDestroy() {
    console.log('SearchService destroy');
    if (this.searchSub) {
      this.searchSub.unsubscribe();
    }
    this.servicesSubject.complete();
    this.servicesSubject.unsubscribe();
  }

  private createSearchParams(): SearchParams {
    const searchParams: SearchParams = {
      query: this.queryTerm,
      targetGroup: this.targetGroup
    };

    if (this.selectedCategories.length > 0) {
      searchParams.categories = this.selectedCategories;
    }
    if (this.selectedIndustries.length > 0) {
      searchParams.industries = this.selectedIndustries;
    }
    return searchParams;
  }

  public loadFirstPage(searchType) {
    // console.log('loadFirstPage ', 'queryTerm:', this.queryTerm, 'targetGroup:', this.targetGroup,
    //   'selectedIndustries:', this.selectedIndustries, 'selectedCategories:', this.selectedCategories);
    this.page = 1;
    this.searchType = searchType;
    this.search(this.page);

    this.loadTags();
    this.loadCategories();
    this.loadIndustries();
  }

  public loadNextPage() {
    this.searchType = 'LoadNextPage';
    if (this.lastPage) {
      console.log('no more pages to load');
    } else {
      this.page = this.page + 1;
      console.log('loadNextPage page:' + this.page);
      this.search(this.page);
    }
  }

  private search(loadPage: number) {

    const searchParams = this.createSearchParams();
    const variables = {
      searchParams,
      pagination: {
        fromPage: loadPage,
        pageSize: this.pageSize
      }
    };

    this.previousQueryTerm = this.queryTerm;
    if (!this.searchQuery) {
      this.searchQuery = this.graphql.watchQuery({
        query: gql`
          query search($searchParams: SearchParamsInput, $pagination: SearchPageableInput) {
            search(searchParams: $searchParams, pageable: $pagination) {
              totalNumberOfItems
              totalNumberOfPages
              numberOfItemsOnPage
              page
              lastPage
              id
              result {
                rank
                id
                identifier
                title
                imageUrl
                shortDescription
              }
            }
          }
        `,
        variables
      });

      this.searchResponse = this.searchQuery.valueChanges
        .pipe(
          map(res => res.data['search'])
        );

      this.searchSub = this.searchQuery.valueChanges.subscribe(
        res => {
          const searchResult = res.data['search'];
          if (searchResult.page === 1) {
            this.servicesSubject.next(searchResult.result);
          } else {
            this.servicesSubject.next(
              this.servicesSubject.value.concat(searchResult.result));
          }

          this.lastPage = searchResult.lastPage;
          this.totalNumberOfItems = searchResult.numberOfItemsOnPage;
          if (!res.loading && searchResult.page === 1) {
            const searchParams2 = this.createSearchParams();
            this.tagManager.eventTrack('On search', {
              event: 'siteSearch', gtmCustom: {
                searchType: this.searchType,
                searchTerm: searchParams2.query,
                numberOfSiteSeachResults: searchResult.totalNumberOfItems,
                targetGroup: searchParams2.targetGroup,
                categories: searchParams2.categories,
                industries: searchParams2.industries
              }
            });
          }
        },
        error => this.servicesSubject.error(error)
      );
    } else {
      this.searchQuery.refetch(variables);
    }
  }

  private loadTags() {
    const searchParams = this.createSearchParams();

    if (this.tagsQuery) {
      this.tagsQuery.refetch({
        searchParams,
        maxNrOfTags: this.maxNrOfTags
      });
    } else {
      this.tagsQuery = this.graphql.watchQuery({
        query: gql`query tagCountForSearch($searchParams: SearchParamsInput, $maxNrOfTags: Int) {
          tagCountForSearch(searchParams: $searchParams, maxNrOfTags: $maxNrOfTags){
            id
            tag
            count
          }
        }`,
        variables: {
          searchParams,
          maxNrOfTags: this.maxNrOfTags
        }
      });

      this.tagsResponse = this.tagsQuery.valueChanges.pipe(
        map(res => res.data.tagCountForSearch.filter(tag => !this.queryTerm.includes(tag.tag)))
      );
    }
  }

  private loadIndustries() {
    const searchParams = this.createSearchParams();

    if (this.industriesQuery) {
      this.industriesQuery.refetch({searchParams});
    } else {
      this.industriesQuery = this.graphql.watchQuery({
        query: gql`
          query industryCountForSearch($searchParams: SearchParamsInput) {
            industryCountForSearch(searchParams: $searchParams) {
              id
              identifier
              title
              description
              count
            }
          }`,
        variables: {searchParams}
      });

      this.industriesResponse = this.industriesQuery.valueChanges.pipe(
        map(res => res.data['industryCountForSearch'])
      );
    }
  }


  private loadCategories() {
    const searchParams = this.createSearchParams();
    if (this.categoriesQuery) {
      this.categoriesQuery.refetch({searchParams});
    } else {
      this.categoriesQuery = this.graphql.watchQuery({
        query: gql`
          query categoryCountForSearch($searchParams: SearchParamsInput) {
            categoryCountForSearch(searchParams: $searchParams) {
              id
              identifier
              name
              description
              parentIdentifier
              count
            }
          }`,
        variables: {searchParams}
      });

      this.categoriesResponse = this.categoriesQuery.valueChanges.pipe(
        map(res => res.data['categoryCountForSearch']),
        map(categories => this.buildTree(categories, {identifier: null})));
    }
  }

  private buildTree(categories: CategoryCount[], parent: CategoryCount): CategoryCount[] {
    parent.subs = categories.filter(category => category.parentIdentifier === parent.identifier);
    parent.subs.forEach(child => this.buildTree(categories, child));
    return parent.subs;
  }

  public selectTag(tag: TagCount) {
    if (!this.queryTerm || this.queryTerm.length < 1) {
      this.queryTerm = tag.tag;
    } else {
      this.queryTerm = this.queryTerm + ' ' + tag.tag;
    }
  }

  public selectedIndustry(industry: IndustryCount, checked: boolean) {
    if (checked && !this.selectedIndustries.includes(industry.identifier)) {
      this.selectedIndustries.push(industry.identifier);
    } else if (!checked) {
      this.selectedIndustries = this.selectedIndustries.filter(item => item !== industry.identifier);
    }
    this.tagManager.eventTrack('On industry check', {
      event: 'siteSearchIndustriClick', gtmCustom: {
        industry: industry.title,
        checked: checked
      }
    });
    this.loadFirstPage('changedIndustry');
  }

  public selectedCategory(category: CategoryCount, checked: boolean) {
    if (checked && !this.selectedCategories.includes(category.identifier)) {
      this.selectedCategories.push(category.identifier);
    } else if (!checked) {
      this.selectedCategories = this.selectedCategories.filter(item => item !== category.identifier);
    }
    this.tagManager.eventTrack('On category check', {
      event: 'siteSearchCategoryClick', gtmCustom: {
        category: category.name,
        checked: checked
      }
    });
    this.loadFirstPage('changedCategory');
  }
}

export interface SearchResult {
  totalNumberOfItems: number;
  totalNumberOfPages: number;
  numberOfItemsOnPage: number;
  page: number;
  lastPage: boolean;
  result: SearchItem[];
  id: string;
}

export interface SearchItem {
  rank: number;
  id: string;
  identifier;
  string;
  title: string;
  imageUrl: string;
  shortDescription: string;
}

export interface SearchParams {
  query?: string;
  targetGroup: string;
  categories?: string[];
  industries?: string[];
}

interface TagResult {
  tagCountForSearch: TagCount[];
}
export interface TagCount {
  id?: string;
  tag: string;
  count: number;
}

export interface IndustryCount {
  id?: string;
  identifier: string;
  title: string;
  description: string;
  count: number;
}

export interface CategoryCount {
  id?: string;
  identifier?: string;
  name?: string;
  description?: string;
  parentIdentifier?: string;
  count?: number;
  subs?: CategoryCount[];
}
