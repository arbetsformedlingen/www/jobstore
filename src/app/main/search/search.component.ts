import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { NguCarouselConfig, NguCarouselStore} from '@ngu/carousel';
import {Angulartics2GoogleTagManager} from 'angulartics2/gtm';
import {Meta, Title} from '@angular/platform-browser';
import {Apollo} from 'apollo-angular';
import {SearchService, TagCount} from './search.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  public carouselTags: NguCarouselConfig;
  private carouselToken: string;

  public placeholder = '../../../assets/images/rectangle-16-copy@3x.png';

  public throttle = 300;
  public scrollDistance = 1;

  public industryChecked = {};
  public categoryChecked = {};

  constructor(
    private tagManager: Angulartics2GoogleTagManager,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title,
    private meta: Meta,
    public searchService: SearchService) {
  }

  ngOnInit() {

    this.carouselTags = {
      grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
      slide: 6,
      speed: 600,
      animation: 'lazy',
      point: {
        visible: false
      },
      touch: true,
      easing: 'ease-in'
    };

    this.title.setTitle('Jobtech Store – Hitta rätt tjänst för jobb, utbildning och coachning – Sök');
    this.meta.addTag({ name: 'description', content: 'Här hittar du tjänster för jobb, utbildning och coachning. Sök med egna taggar eller använd dig av våra förslag, och smalna av efter bransch eller typ av tjänst. Öppet, kostnadsfritt och oberoende.'});

    this.route.queryParams.subscribe(params => {
      this.searchService.queryTerm = params['queryTerm'] || '';
      this.searchService.targetGroup = params['targetGroup'] || 'WORKER';
      let categories = params['categories'] || [];
      let industries = params['industries'] || [];

      if (!Array.isArray(categories)) {
        categories = [categories];
      }
      if (!Array.isArray(industries)) {
        industries = [industries];
      }
      this.searchService.selectedCategories = categories;
      this.searchService.selectedIndustries = industries;

      this.categoryChecked = {};
      this.searchService.selectedCategories.forEach(selected => {
        this.categoryChecked[selected] = true;
      });

      this.industryChecked = {};
      this.searchService.selectedIndustries.forEach(selected => {
        this.industryChecked[selected] = true;
      });

      this.doSearch('OnLoad');
    });
  }

  goBack() {
    window.history.back();
  }

  onKeydown(event) {
    if (event.key === 'Enter') {
      this.doSearch('HitEnterInSearchTerm');
    }
  }

  public doSearch(searchType): void {
    this.searchService.loadFirstPage(searchType);
  }

  public onTagClick(tag: TagCount) {
    this.tagManager.eventTrack('On search tag click', {
      event: 'siteSearchTagClick', gtmCustom: {
        tag: tag.tag,
        tagCount: tag.count
      }
    });

    this.searchService.selectTag(tag);
    this.doSearch('tagClick');
  }

  onServiceClick(service, hitIndex) {
    this.tagManager.eventTrack('On search result click', {
      event: 'siteSearchResultClick', gtmCustom: {
        searchTerm: this.searchService.queryTerm,
        numberOfSiteSeachResults: this.searchService.totalNumberOfItems,
        resultPage: this.searchService.page,
        hitIndex: hitIndex,
        hitRank: service.rank,
        serviceIdentifyer: service.identifier,
        serviceTitle: service.title
      }
    });
  }

  public setTargetGroup(value) {
    this.searchService.targetGroup = value;
    console.log('searchService.targetGroup:', this.searchService.targetGroup);
    this.searchService.selectedIndustries = [];
    this.searchService.selectedCategories = [];
    this.industryChecked = {};
    this.categoryChecked = {};

    this.tagManager.eventTrack('On search target group click', {
      event: 'siteSearchTargetGroupClick', gtmCustom: {
        targetGroup: this.searchService.targetGroup
      }
    });

    this.doSearch('SelectTargetGroup');
  }

  ngOnDestroy() {
  }

}

