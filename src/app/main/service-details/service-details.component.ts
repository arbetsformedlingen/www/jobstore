import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {map, tap} from 'rxjs/operators';
import { KeycloakService } from '../../shared/services/keycloak/keycloak.service';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.scss']
})
export class ServiceDetailsComponent implements OnInit {

  public serviceAsync: Observable<any>;
  public others: Array<Other>;
  public placeholder = '../../../assets/images/rectangle-16-copy@3x.png';
  public isTagPanelCollapsed = true;
  public tagOverflow = 4;
  public showAllOther = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private graphql: Apollo,
    public keycloakService: KeycloakService,
    private sanitizer: DomSanitizer) { }

  public doTheBack = function() {
    window.history.back();
  };

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const identifier = params.get('identifier');

      this.serviceAsync = this.graphql.query(
        { query: gql`
            query getService($id: ID) {
              service(identifier: $id) {
                identifier
                imageUrl
                link
                title
                shortDescription
                description
                targetGroup
                categories {
                  name
                }
                industries {
                  title
                }
                tags
                organisation {
                  name
                  services{
                    identifier
                    imageUrl
                    title
                    shortDescription
                  }
                }
                youtube
                socialTwitter
                socialInstagram
                socialLinkedIn
                published
                banned
              }
            }`,
          variables: {id: identifier}
        }
      ).pipe(
        map(res => res.data['service']),
        tap(service => {
          const services: Array<Other> = service['organisation']['services'];
          this.others = services.filter(svn => svn.identifier !== identifier);
        })
      );
    });
  }

  transform(url) {
    if (url.indexOf('watch?v=') !== -1) {
      const id = url.split('watch?v=')[1];
      return this.sanitizer.bypassSecurityTrustResourceUrl(('http://www.youtube.com/embed/' + id));
    } else if (url.indexOf('.be/') !== -1) {
      const id = url.split('.be/')[1];
      return this.sanitizer.bypassSecurityTrustResourceUrl(('http://www.youtube.com/embed/' + id));
    } else {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }

  descColums(description) {
    if (description.length > 255) {
      return 'descColums-2';
    }
    return 'descColums-1';
  }

  gotoEdit(serviceId) {
    this.router.navigate(
      ['/backstage/service', serviceId],
      { queryParams: { callback: '/service/' + serviceId } });
  }

}


interface Other {
  identifier: string;
  imageUrl: string;
  title: string;
  shortDescription: string;
};
