import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {NguCarouselModule} from '@ngu/carousel';
import {Angulartics2Module} from 'angulartics2';
import {MaterialModule} from '../material';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {MainRoutingModule} from './main-routing.module';
import {MainComponent} from './main.component';
import {ServiceItemComponent} from './service-item/service-item.component';
import {AboutComponent} from './about/about.component';
import {HomeComponent} from './home/home.component';
import {MenuComponent} from './layouts/menu/menu.component';
import {FooterComponent} from './layouts/footer/footer.component';
import {ServiceDetailsComponent} from './service-details/service-details.component';
import {TruncatePipe} from '../shared/pipes/truncate.pipe';
import {ImgFallbackModule} from 'ngx-img-fallback';
import {LinkHttp} from '../shared/pipes/linkHttp.pipe';
import {SearchComponent} from './search/search.component';
import {FiltersComponent} from './filters/filters.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AccessibilityComponent } from './accessibility/accessibility.component';


@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    HttpClientModule,
    NgbModule,
    NguCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    ImgFallbackModule,
    Angulartics2Module,
    MaterialModule,
    InfiniteScrollModule,
    DragDropModule
  ],
  declarations: [
    MainComponent,
    FooterComponent,
    MenuComponent,
    HomeComponent,
    AboutComponent,
    AccessibilityComponent,
    ServiceItemComponent,
    ServiceDetailsComponent,
    TruncatePipe,
    LinkHttp,
    SearchComponent,
    FiltersComponent
  ],
})
export class MainModule {
}
