import { Component, Input, OnInit } from '@angular/core';
import { ServiceSummary } from '../../shared/model/service-summary';
import { Router } from '@angular/router';
import { MetadataService} from '../../shared/services/metadata.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'service-item',
  templateUrl: './service-item.component.html',
  styleUrls: ['./service-item.component.scss']
})
export class ServiceItemComponent implements OnInit {

  public placeholder = '../../../assets/images/rectangle-16-copy@3x.png';

  @Input() serviceSummary: ServiceSummary;
  public imageUrl: Observable<string>;

  constructor(private router: Router,
              private metadataService: MetadataService) {
  }

  ngOnInit() {
    this.imageUrl = this.metadataService.metadata
      .pipe(
        map ((meta, index) => {
          // console.log('Got a metadata object ', meta, this.serviceSummary);
          if (this.serviceSummary.imgUrl && this.serviceSummary.imgUrl.startsWith('/')) {
            return meta.imageStoreUrl + this.serviceSummary.imgUrl;
          }
          return this.serviceSummary.imgUrl;
        }));
  }

  openDetailsView(): void {
    this.router.navigate(['/service', this.serviceSummary.identifier]);
  }


}
