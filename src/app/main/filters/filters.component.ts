import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {CategoryCount, IndustryCount, SearchService} from '../search/search.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnChanges {

  private industryCheckedValue = {};
  private categoryCheckedValue = {};
  @Output() public industryCheckedChange = new EventEmitter();
  @Output() public categoryCheckedChange = new EventEmitter();
  @Output() public filterChanged = new EventEmitter();

  @Input()
  public get industryChecked() {
    return this.industryCheckedValue;
  }
  public set industryChecked(value) {
    this.industryCheckedValue = value;
    this.industryCheckedChange.emit(this.industryCheckedValue);
  }

  @Input()
  public get categoryChecked() {
    return this.categoryCheckedValue;
  }
  public set categoryChecked(value) {
    this.categoryCheckedValue = value;
    this.categoryCheckedChange.emit(this.categoryCheckedValue);
  }

  constructor(
    public searchService: SearchService,
    config: NgbDropdownConfig
  ) {
    config.autoClose = 'outside';
    config.placement = 'bottom-left';
  }

  ngOnChanges() {
    // console.log('ngOnChanges');
  }

  public toggleIndustry(event, industry: IndustryCount): void {
    this.industryChecked[industry.identifier] = event;
    this.searchService.selectedIndustry(industry, event);
    this.fireChangeAndRefresh();
  }

  public toggleCategory(event, category: CategoryCount): void {
    this.categoryChecked[category.identifier] = event;
    this.searchService.selectedCategory(category, event);
    this.fireChangeAndRefresh();
  }

  private fireChangeAndRefresh() {
    this.filterChanged.emit();
  }

}
