import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuService } from '../../../shared/services/menu.service';
import { Router } from '@angular/router';
import {Observable} from 'rxjs';
import {Organisation} from '../../../shared/model/organisation';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {KeycloakService} from '../../../shared/services/keycloak/keycloak.service';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public isNavbarCollapsed = false;
  public organisation: Observable<Organisation>;

  constructor(private menuService: MenuService,
              public keycloakService: KeycloakService,
              private userService: UserService,
              private router: Router) { }

  static getHostUrl() {
    return window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
  }


  ngOnInit() {
    this.organisation = this.userService.userOrganisation;
  }

  doLogout() {
    console.log('doLogout');
    this.keycloakService.client().logout({redirectUri: MenuComponent.getHostUrl() });
  }

  doLogin() {
    console.log('doLogin');
    this.keycloakService.client().login()
      .success(result => console.log('Logged in succesful', result))
      .error(error => console.log('Login failed ', error));
  }

  doRegister() {
    console.log('doRegister');
    this.keycloakService.client().register();
  }

  toggelNavbar() {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  toggleMenu() {
    this.menuService.toggleMenu();
  }

  hideMenu() {
    this.menuService.hideMenu();
  }

  isVisible() {
    return this.menuService.isVisible();
  }


  goToSearch() {
    this.router.navigate(['/search']);
  }


}
