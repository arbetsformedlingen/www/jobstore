import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {MetadataService} from '../../../shared/services/metadata.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public webVersion: string = environment.webVersion;

  constructor(public metadataService: MetadataService) {
  }

  ngOnInit() {
  }
}
