import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../shared/services/menu.service';
import { Router } from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public mobile = true;

  constructor(private menuService: MenuService,
              private router: Router,
              private title: Title,
              private meta: Meta) {
  }

  ngOnInit() {
    this.title.setTitle('Jobtech Store – Tjänster inom jobb, utbildning och coachning – Star');
    this.meta.addTag({ name: 'description', content: 'Vi samlar tjänster och verktyg för jobb, utbildning och coachning, oavsett om du vill utvecklas, byta jobb eller rekrytera personal. Anslut din tjänst och nå ut till en större och mer relevant målgrupp. Öppet, kostnadsfritt och oberoende.'});
    if (window.screen.width > 768) {
      this.mobile = false;
    }
    this.menuService.hideMenu();
  }

}
