import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {trigger, animate, style, group, animateChild, query, stagger, transition} from '@angular/animations';
import { Angulartics2GoogleTagManager, GoogleTagManagerDefaults } from 'angulartics2/gtm';
import {environment} from '../environments/environment';
import {MetadataService} from './shared/services/metadata.service';

export const routerTransition = trigger('routerTransition', [
  transition('front => back', [
    query(':enter, :leave', style({position: 'fixed', width: '100%'}), {optional: true}),
    group([
      query(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))], {optional: true}),
      query(':leave', [
        style({transform: 'translateX(0%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))], {optional: true}),
    ])
  ]),
  transition('back => front', [
    query(':enter, :leave', style({position: 'fixed', width: '100%'}), {optional: true}),
    group([
      query(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))], {optional: true}),
      query(':leave', [
        style({transform: 'translateX(0%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(100%)'}))], {optional: true}),
    ])
  ])
]);


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routerTransition],
})
export class AppComponent implements OnInit {

  public isProd = true;
  public env: string;

  private static scrollToTopOfPage() {
    window.scrollTo(0, 0);
  }

  constructor(private router: Router,
              public metadataService: MetadataService,
              private angulartics2GoogleTagManager: Angulartics2GoogleTagManager) {
  }

  ngOnInit() {
    this.angulartics2GoogleTagManager.startTracking();

    this.metadataService.metadata.subscribe(meta => {
      this.env = meta.environment;
      this.isProd = !this.env || this.env.toLowerCase().includes('production');
    });

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        AppComponent.scrollToTopOfPage();
      }
    });
  }

  getStage(outlet) {
    return outlet.activatedRouteData.stage;
  }

}
