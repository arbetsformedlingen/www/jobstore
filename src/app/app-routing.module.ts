import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MainModule} from './main/main.module';
import {BackstageModule} from './backstage/backstage.module';
import {GoneComponent} from './shared/gone/gone.component';

const routes: Routes = [
  {
    path: 'backstage', data: { stage: 'back' },
    loadChildren: () => BackstageModule
  },
  {
    path: '', data: { stage: 'front' },
    loadChildren: () => MainModule
  },
  { path: '**', component: GoneComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {
}
