import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import {BackstageComponent} from './backstage.component';
import {ProfileComponent} from './account/profile/profile.component';
import {KeycloakGuard} from '../shared/services/keycloak/keycloak.guard';
import {JobServiceListComponent} from './account/job-service-list/job-service-list.component';
import {JobServiceEditorComponent} from './account/job-service-editor/job-service-editor.component';
import {OrganisationComponent} from './account/organisation/organisation.component';
import {InvitesComponent} from './invites/invites.component';
import {CodeOfConductComponent} from './code-of-conduct/code-of-conduct.component';
import {AddOrganisationComponent} from './onboarding/add-organisation/add-organisation.component';
import {WizardComponent} from './onboarding/wizard/wizard.component';
import {AddServiceComponent} from './onboarding/add-service/add-service.component';

const backstageRoutes: Routes = [
  {path: '', component: BackstageComponent,
    children: [
      {path: '', component: HomeComponent},
      {path: 'code-of-conduct', component: CodeOfConductComponent},
      {path: 'onboarding', component: WizardComponent},
      {path: 'onboard/org', component: AddOrganisationComponent},
      {path: 'onboard/svn', component: AddServiceComponent},
      {path: 'invites', component: InvitesComponent, canActivate: [KeycloakGuard]},
      {path: 'home', redirectTo: '', pathMatch: 'full'},
      {path: 'profile', component: ProfileComponent, canActivate: [KeycloakGuard]},
      {path: 'services', component: JobServiceListComponent, canActivate: [KeycloakGuard]},
      {path: 'service/add', component: JobServiceEditorComponent, canActivate: [KeycloakGuard]},
      {path: 'service/:identifier', component: JobServiceEditorComponent, canActivate: [KeycloakGuard]},
      {path: 'organisation', component: OrganisationComponent, canActivate: [KeycloakGuard]},
      {path: 'profile', component: ProfileComponent, canActivate: [KeycloakGuard]},
      {path: '**', component: HomeComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(backstageRoutes)],
  exports: [RouterModule]
})
export class BackstageRoutingModule { }
