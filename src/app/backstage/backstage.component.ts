import { Component, OnInit,  ElementRef, ViewChild, AfterContentChecked } from '@angular/core';
import {UserService} from '../shared/services/user.service';
import {Meta} from '@angular/platform-browser';
import {MetadataService} from '../shared/services/metadata.service';

@Component({
  selector: 'app-backstage',
  templateUrl: './backstage.component.html',
  styleUrls: ['./backstage.component.scss']
})
export class BackstageComponent implements OnInit, AfterContentChecked {

  @ViewChild('base') elementView: ElementRef;
  public viewHeight: number;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    this.viewHeight = this.elementView.nativeElement.offsetHeight + 1000;
  }

}
