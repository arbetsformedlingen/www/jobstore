import { Component, OnInit, ElementRef } from '@angular/core';
import {KeycloakService} from '../../shared/services/keycloak/keycloak.service';
import {Observable} from 'rxjs';
import {Organisation} from '../../shared/model/organisation';
import {OrganisationService} from '../../shared/services/organisation.service';
import {UserService} from '../../shared/services/user.service';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-backstage-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private title: Title,
    private meta: Meta,
    public keycloakService: KeycloakService,
    public userService: UserService) { }

  ngOnInit() {
    this.title.setTitle('Jobtech Store – Anslut din tjänst för jobb, utbildning och coachning – Registrera organisation');
    this.meta.addTag({ name: 'description', content: 'Anslut din tjänst till vår plattform för jobb, utbildning och coachning, och nå ut till en större och mer relevant målgrupp. Vi samlar tjänster och verktyg, oavsett om den hjälper din målgrupp att utvecklas, hitta nytt eller rekrytera personal. Öppet, kostnadsfritt och oberoende.'});

  }

  doLogin() {
    console.log('doLogin');
    this.keycloakService.client().login();
  }

  doRegister() {
    console.log('doRegister');
    this.keycloakService.client().register();
  }

  updateOrg(organisation: Organisation) {
    this.userService.updateOrganisation(organisation);
  }

  onboardingCompleted(organisation: Organisation) {
    this.userService.updateOrganisation(organisation);
    window.scroll(0, 0);
  }
}
