import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {ToastrService} from 'ngx-toastr';
import {v4 as uuid} from 'uuid';
import {Organisation} from '../../../shared/model/organisation';
import {Category} from '../../../shared/model/category';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {FormControl, FormGroup, NgForm} from '@angular/forms';
import {Registration} from '../../../shared/model/registration';
import {JobServiceRegistrationService} from '../../../shared/services/job-service-registration.service';
import {CategoryService} from '../../../shared/services/category.service';
import {WizardModelService} from '../wizard/wizard-model.service';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {Industry} from '../../../shared/model/industry';
import { CategoriesComponent } from '../../categories/categories.component';
import {map} from 'rxjs/operators';


@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss']
})
export class AddServiceComponent implements OnInit, OnChanges {

  @ViewChild(CategoriesComponent)
  categoryComp: CategoriesComponent;

  public selectedCategories = [100];

  public asyncIndustries: Observable<Industry[]>;
  public selectedIndustries = {};

  @Output() public back = new EventEmitter();
  @Output() public ready = new EventEmitter();

  @ViewChild('childComponent') imageComp;

  public logoUrlModel;
  private key = '5b769159827bd6820c7fc772'; // TODO: HIDE THIS IN PROD!!!!
  private ogScrapeUrl = ' https://opengraph.io/api/1.1/site/';
  public loadingData = false;

  public progressType: string;
  public validators: any[] = [this.minCharLenght, this.maxCharLenght];
  public errorMessages = {
    'minCharLenght': 'Taggarna behöver vara minst 2 tecken långa.',
    'maxCharLenght': 'Taggarna får inte vara längre än 100 tecken.'
  };

  constructor(
    public model: WizardModelService,
    private registrationService: JobServiceRegistrationService,
    public categoryService: CategoryService,
    private toastrService: ToastrService,
    private http: HttpClient,
    private apl: Apollo) {
  }

  ngOnInit() {
     this.asyncIndustries = this.apl.watchQuery<any>({
        query: gql`
        {
          industries {
            identifier
            title
            description
            usedByNoOfServices
          }
        }
        `
     })
     .valueChanges
     .pipe(
       map(res => res.data['industries'])
      );
  }

  ngOnChanges() {
  }

  onBackClicked() {
    this.back.emit();
  }

  onReady(form: NgForm) {
    if (form.form.invalid) {
      const invalidControl = this.findInvalidControls(form);
      if (invalidControl) {
        console.log(invalidControl);
        this.scrollToId(invalidControl);
      }
      this.validateAllFormFields(form);
      this.toastrService.error('Något är inte ordentligt ifyllt! Kolla över och försök igen.');
      return false;
    }
    this.model.service['category'] = this.model.service.categories[0];
    this.ready.emit(this.model.service);
  }

  validateAllFormFields(formGroup: NgForm) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.control.get(field);
      control.markAsDirty({ onlySelf: true });
    });
  }

  findInvalidControls(form: NgForm) {
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        return name;
      }
    }
  }

  scrollToId(id: string) {
    const elmnt = document.getElementById(id);
    elmnt.scrollIntoView({behavior: 'smooth', block: 'center', inline: 'nearest'});
  }

  newImageUploade(image: File) {
    console.log('new image to upload ', image);

    this.registrationService.addImage(this.model.service, image)
      .subscribe(res => {
        console.log('SUCCESS', res);
        this.model.service.logoUrl = this.logoUrlModel = `${res['link']}`;
        console.log('this.logoUrl=', this.model.service.logoUrl);
      }, error => {
        console.log('FAILED TO UPLOAD: ', error);
        this.toastrService.error('Något gick fel med bilden. Försök igen eller testa med en annan bild.');
      });
  }

  imageDeleted(imageName: string) {
    this.registrationService.removeImage(this.model.service)
      .subscribe(res => {
        this.model.service.logoUrl = this.logoUrlModel = '';
      }, error => {
        this.toastrService.error('Det gick inte att ta bort bilden. Försök igen!');
      });
  }

  changeCategory(object) {
    this.model.service.categories = this.model.service.categories.filter(category => category.identifier !== object.identifier);
    if (object.event.currentTarget.checked) {
      this.model.service.categories.push(
        {identifier: object.identifier, name: null, description: null, targetEmployer: null, targetWorker: null, subs: null});
    }
  }

  changeIndustry(identifier: string, event) {
    this.model.service.industries = this.model.service.industries.filter(industry => industry.identifier !== identifier);
    if (event.currentTarget.checked) {
      this.model.service.industries.push(new Industry(identifier));
    }
  }

  resetCategories() {
    this.model.service.categories = [];
    this.categoryComp.selectedCategories = {};
  }

  public  tagProgress() {
    if (this.model.service.tags.length > 7) {
      this.progressType = 'success';
    } else if (this.model.service.tags.length > 2) {
      this.progressType = 'warning';
    } else {
      this.progressType = 'danger';
    }
  }

  private minCharLenght(control) {
    if (control.value.length < 2) {
      return {
        'minCharLenght': true
      };
    } else {
      return null;
    }
  }

  private maxCharLenght(control) {
    if (control.value.length > 100) {
      return {
        'maxCharLenght': true
      };
    } else {
      return null;
    }
  }

  public convertToLower(tag: String): Observable<String> {
    return of(tag.toLowerCase());
  }

  public log(message) {
    console.log('Selected ', message);
  }

  public getServiceData(url) {
    this.loadingData = true;
    if (url.length > 0) {
      const checkedUrl = this.checkURL(url);
      const apiCall = `${this.ogScrapeUrl}${checkedUrl}?app_id=${this.key}`;
      // TODO ADD CHECKS IF KEYS ARE UNDEFINED OR NOT
      this.http.get(apiCall).subscribe(result => {
          if (result['error']) {
            console.log('%cMAX AMOUNT OF API CALLS REACHED!', 'color: red; font-size: 20px');
            console.log('%cINFO: ' + result['error'].message, 'font-sixe: 16px;');
          } else {
            this.model.service.shortDescription = result['hybridGraph'].title;
            this.model.service.description = result['hybridGraph'].description;
            this.scrollToId('description');
            if (result['hybridGraph'].image && result['hybridGraph'].image !== result['hybridGraph'].favicon) {
              setTimeout(() => {
                this.imageComp.triggerModal(result['hybridGraph'].image);
              }, 1000);
            }
          }
          this.loadingData = false;
        },
        error => {
          console.log(error);
        });
    }
  }

  doEncodeURI(url) {
    return encodeURI(url);
  }

  private checkURL(urlToCheck): String {
    let string = urlToCheck;
    if (string && !/^(https?):\/\//i.test(string) && ('http://'.indexOf(string) === -1 && 'https://'.indexOf(string) === -1)) {
      string = 'http://' + string;
    }
    return string;
  }
}


