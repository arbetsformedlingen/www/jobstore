import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import { v4 as uuid } from 'uuid';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {Organisation} from '../../../shared/model/organisation';
import {ToastrService} from 'ngx-toastr';
import {Registration} from '../../../shared/model/registration';
import {NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap, tap} from 'rxjs/operators';
import {WizardModelService} from '../wizard/wizard-model.service';
import {text} from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-add-organisation',
  templateUrl: './add-organisation.component.html',
  styleUrls: ['./add-organisation.component.scss']
})
export class AddOrganisationComponent implements OnInit, OnChanges {

  @Output() public ready = new EventEmitter();
  @Output() public joined = new EventEmitter();

  orgToNameFormatter = (org) => {
    // console.log('orgToNameFormatter: ', org);
    if (this.isString(org)) {
      return org;
    }
    return org.name;
  }

  formatter = (x: {name: string}) => {
    // console.log('formatter: ', x);
    return x.name;
  }

  typeaheadOrganisations = (text$: Observable<Organisation>) => {
    // console.log('typeaheadOrganisations: ')
    return text$.pipe(
      // tap(term => console.log('typeaheadOrganisations: ', term)),
      debounceTime(300),
      distinctUntilChanged(),
      // tap(term => console.log('typeaheadOrganisations passed: ', term)),
      switchMap(term => this.orgService.findNameOrganisations(term) ));
  }

  private isString(value) {
    return typeof value === 'string' || value instanceof String;
  }

  constructor(
    public model: WizardModelService,
    private orgService: OrganisationService,
    private toastrService: ToastrService) { }

  ngOnInit() {
  }

  ngOnChanges() {
  }

  next(form: NgForm) {
    if (form.form.invalid) {
      const invalidControl = this.findInvalidControls(form);
      if (invalidControl) {
        this.scrollToId(invalidControl);
      }
      this.validateAllFormFields(form);
      this.toastrService.error('Något är inte ordentligt ifyllt! Kolla över och försök igen.');
      return false;
    }
    this.ready.emit();
  }

  findInvalidControls(form: NgForm) {
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        return name;
      }
    }
  }

  scrollToId(id: string) {
    const elmnt = document.getElementById(id);
    elmnt.scrollIntoView({behavior: 'smooth', block: 'center', inline: 'nearest'});
  }

  validateAllFormFields(formGroup: NgForm) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.control.get(field);
      control.markAsDirty({ onlySelf: true });
    });
  }


  newImageUploade(image: File) {

    this.orgService.addOrganisationImage(this.model.organisation, image)
      .subscribe(res => {
        this.model.organisation.logoUrl = `${res['link']}`;
      }, error => {
        console.log('FAILED TO UPLOAD: ', error);
        this.toastrService.error('Något gick fel med bilden. Försök med en annan eller testa igen senare.');
      });
  }

  imageDeleted(imageName: string) {

    this.orgService.removeImage(this.model.organisation)
      .subscribe(res => {
        this.model.organisation.logoUrl = '';
      }, error => {
        this.toastrService.error('Det gick inte att ta bort bilden. Försök igen!');
      });
  }

  public doJoinOrganisation() {
    this.orgService.requestMembership(this.model.joinOrganisation).subscribe(result => { this.joined.emit(result); });
  }

  public selectOrganisation(event) {
    this.model.joinOrganisation = event.item;
  }

  public onChange(event) {
    this.model.joinOrganisation = null;
  }

  public cancelJoinOrganisation() {
    this.model.joinOrganisation = null;
  }
}
