import {Directive} from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  AsyncValidatorFn,
  FormControl,
  NG_ASYNC_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn
} from '@angular/forms';
import {Observable, of, EMPTY} from 'rxjs';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {debounceTime, distinctUntilChanged, map, switchMap, take} from 'rxjs/operators';

//https://www.concretepage.com/angular-2/angular-custom-async-validator-example
//https://mobiarch.wordpress.com/2017/12/26/angular-async-validator/
@Directive({
  selector: '[app-uniqueOrganisationName][formControlName],[app-uniqueOrganisationName][formControl],[app-uniqueOrganisationName][ngModel]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: UniqueOrganisationNameValidatorDirective,
      multi: true
    }
  ]
})
export class UniqueOrganisationNameValidatorDirective implements AsyncValidator {
  constructor(private orgService: OrganisationService) {
  }

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    // console.log('value', control.value);
    if (!control.value) {
      return of(null);
    } else {
      if (this.isString(control.value) ) {
        return this.orgService.isNameAvailable(control.value)
          .pipe(
            map(available => {
                // console.log('available', available)
                return available ? null : {'organisationNameNotUnique': true, 'organisationName': control.value};
              }
          ));
      } else {
        return of({'organisationNameNotUnique': true, 'organisationName': control.value['name']});
      }
    }
  }

  private isString(value) {
    return typeof value === 'string' || value instanceof String;
  }
}
