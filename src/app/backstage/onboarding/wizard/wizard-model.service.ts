import { Injectable } from '@angular/core';
import {Organisation} from '../../../shared/model/organisation';
import {Registration} from '../../../shared/model/registration';
import {v4 as uuid} from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class WizardModelService {

  public activeStep: number;
  public stepOneDone: boolean;
  public stepTwoDone: boolean;

  public organisation: Organisation;
  public service: Registration;

  public joinOrganisation: Organisation;
  public codeOfConduct = false;
  public codeOfConductJoin = false;

  constructor() {
    this.reset();
  }

  reset() {
    this.activeStep = 1;
    this.stepOneDone = false;
    this.stepTwoDone = false;
    this.joinOrganisation = null;
    this.codeOfConduct = false;
    this.codeOfConductJoin = false;

    this.organisation = new Organisation();
    this.organisation.identifier = uuid();
    this.organisation.allowDirectMembership = true;

    this.service = new Registration();
    this.service.identifier = uuid();
    this.service.organisation =  this.organisation;
    this.service.targetGroup = 'WORKER';
    this.service.categories = [];
    this.service.tags = [];
    this.service.published = true;
  }
}
