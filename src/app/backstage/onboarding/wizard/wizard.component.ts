import {Component, EventEmitter, OnInit, Output, AfterContentChecked, ViewChild, ElementRef} from '@angular/core';
import {Organisation} from '../../../shared/model/organisation';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {ToastrService} from 'ngx-toastr';
import {JobServiceRegistrationService} from '../../../shared/services/job-service-registration.service';
import {ServiceSummary} from '../../../shared/model/service-summary';
import {WizardModelService} from './wizard-model.service';

@Component({
  selector: 'app-onboarding-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit, AfterContentChecked {

  @ViewChild('base') elementView: ElementRef;
  @Output() public completed = new EventEmitter();

  public viewHeight: number;

  constructor(
    public model: WizardModelService,
    private orgService: OrganisationService,
    private registrationService: JobServiceRegistrationService,
    private toastrService: ToastrService) { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    this.viewHeight = this.elementView.nativeElement.offsetHeight + 210;
  }

  public setStep(newStep) {
    this.model.activeStep = newStep;
  }

  public onOrganisationReady() {
    this.setStep(2);
    this.model.stepOneDone = true;
    window.scroll(0, 0);
  }

  public onSave() {
    console.log('onSave', this.model.organisation, this.model.service);
    this.model.stepTwoDone = true;
    this.orgService.create(this.model.organisation)
      .subscribe((saved: Organisation) => {
        console.log('Organisation was persisted as ', saved);
        this.model.organisation = saved;
        this.model.service.organisation = this.model.organisation;
        const sub = this.registrationService.create(this.model.service)
          .subscribe(
            (savedService) => {
              console.log('The service  was persisted as ', savedService);
              this.toastrService.success('The service  was persisted as ', savedService.name);
              this.model.service = savedService;
              const listService: ServiceSummary = Object.assign(new ServiceSummary(), savedService);
              listService.categories = this.model.service.categories.map(cat => cat.name);
              this.model.organisation.services = [listService];
              const display = this.model.organisation;
              this.model.reset();
              this.completed.emit(display);
            },
            (error) => {
              console.error('Failed to register the service', error);
              this.toastrService.error('Failed to register the service');
            },
            () => sub.unsubscribe());
      }, error => {
        console.log('Organisation failed persisted.', error);
        this.toastrService.error('Det gick inte att spara organisationen');
      });

  }

  public joinedOrg(organisation: Organisation) {
    this.model.reset();
    this.completed.emit(organisation);
  }
}
