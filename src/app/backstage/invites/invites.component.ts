import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {OrganisationService} from '../../shared/services/organisation.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-invites',
  templateUrl: './invites.component.html',
  styleUrls: ['./invites.component.scss']
})
export class InvitesComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private toastrService: ToastrService,
              private orgService: OrganisationService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log('queryParams code: ', params['code'], params);

      this.orgService.acceptInvite(params['code'])
        .subscribe( org => {
          console.log('response ', org);
          this.toastrService.info('Det gick bra, du är nu administrator för ' + org.name);
          this.router.navigate(['/backstage']);
        }, error => {
          console.log('error ', error);
          this.toastrService.error('Ops något gick fel' + error);
        });
    });

  }

}
