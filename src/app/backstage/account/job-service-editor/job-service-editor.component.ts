import { Component, OnDestroy, OnInit, Input, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { v4 as uuid } from 'uuid';
import { Registration } from '../../../shared/model/registration';
import { JobServiceRegistrationService } from '../../../shared/services/job-service-registration.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { OrganisationService } from '../../../shared/services/organisation.service';
import { CategoryService } from '../../../shared/services/category.service';
import { Category } from '../../../shared/model/category';
import { TagModel } from 'ngx-chips/core/accessor';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {Industry} from '../../../shared/model/industry';
import { CategoriesComponent } from '../../categories/categories.component';
import {map} from 'rxjs/operators';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-job-service-editor',
  templateUrl: './job-service-editor.component.html',
  styleUrls: ['./job-service-editor.component.scss']
})
export class JobServiceEditorComponent implements OnInit, OnDestroy {

  @ViewChild(CategoriesComponent)
  categoryComp: CategoriesComponent;

  private registration: Observable<Registration>;
  private subscription: any;
  private registrationId;
  protected isNew = true;
  private callback;
  public service: Registration;
  protected originalService: Registration;
  protected title: string;
  public tags: string[] = [];
  public asyncIndustries: Observable<Industry[]>;
  public selectedIndustries = {};

  public serviceImage: string;

  private orgSub;
  public progressType: string;
  public validators: any[] = [this.minCharLenght, this.maxCharLenght];
  public errorMessages = {
    'minCharLenght': 'Din tag behöver va minst 2 tecken',
    'maxCharLenght': 'Din tag överstiger gränsen på 100 tecken'
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    protected jobServicesService: JobServiceRegistrationService,
    private orgService: OrganisationService,
    public userService: UserService,
    public categoryService: CategoryService,
    private apl: Apollo) {
  }

  public checkedCat() {
    return true;
  }

  newImageUploade(image: File) {
    console.log('new image to upload ', image, this.service);
      this.jobServicesService.addImage(this.service,  image).subscribe( res => {
      this.service.logoUrl = `${res['link']}`;
      console.log(res);
      this.toastrService.success('Bilden laddades upp för din tjänst!');
    }, error => {
      console.log('FAILED TO UPLOAD: ', error);
      this.toastrService.error('Misslyckades att ladda upp din bild till servern, försök igen senare');
    });
  }

  imageDeleted(imageName: string) {
    console.log('deleting service image,', this.service);
    this.jobServicesService.removeImage(this.service)
      .subscribe(res => {
        this.service.logoUrl = '';
      }, error => {
        this.toastrService.error('Det gick inte att ta bort bilden');
      });
  }

  private minCharLenght(control) {
    if (control.value.length < 2) {
      return {
        'minCharLenght': true
      };
    } else {
      return null;
    }
  }

  private maxCharLenght(control) {
    if (control.value.length > 100) {
      return {
        'maxCharLenght': true
      };
    } else {
      return null;
    }
  }

  private tagProgress() {
    // console.log(event);
    if (this.tags.length > 7) {
      this.progressType = 'success';
    } else if (this.tags.length > 2) {
      this.progressType = 'warning';
    } else {
      this.progressType = 'danger';
    }
  }

  public doEncodeURI(url) {
    return encodeURI(url);
  }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      if (params['callback']) {
        this.callback = params['callback'];
      } else {
        this.callback = '/backstage';
      }
    });

    this.subscription = this.route.params.subscribe(params => {
      this.registrationId = params['identifier'];

      if (this.registrationId) {
        // console.log('Got registrationId: ' + this.registrationId + ' from route');
        this.registration = this.jobServicesService.get(this.registrationId);
        this.registration.subscribe(reg => {
          this.isNew = false;
          this.originalService = reg;
          this.service = new Registration();
          Object.assign(this.service, reg);
          this.tags = this.service.tags;
          this.service.industries.forEach(industry => this.selectedIndustries[industry.identifier] = true);
          this.tagProgress();
          // console.log('loaded', this.service, this.selectedIndustries);
        });
      } else {
        this.service = new Registration();
        this.service.categories = [];
        this.service.identifier = uuid();
        this.service.targetGroup = 'WORKER';
        this.tags = [];
        this.tagProgress();
        console.log('init', this.service);
        this.orgSub = this.orgService.getMyOrganisation().subscribe(
          org => {
            this.service.organisation = {
              identifier: org.identifier,
              name: org.name
            };
          },
          error => {
            this.toastrService.error('Failed to load your organisation!');
            console.error('Failed to load your organisation! ', error);
          }
        );
      }
    });

    this.asyncIndustries = this.apl.watchQuery<any>({
      query: gql`
      {
        industries {
          identifier
          title
          description
          usedByNoOfServices
        }
      }
      `
   })
   .valueChanges
    .pipe(map(res => res.data['industries']));
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDelete() {
    if (confirm('Vill du verkligen radera den här tjänsten?')) {
      const sub = this.jobServicesService.remove(this.registrationId).subscribe(
        () => {
          this.toastrService.success('Tjänsten är nu borttagen.');
          this.router.navigate(['/backstage']);
        },
        (error) => {
          this.toastrService.error('Misslyckades att ta bort tjänsten.');
          console.error('Failed to remove the service', error);
        },
        () => sub.unsubscribe()
      );
    }
  }

  onSave(theForm) {
    if (theForm.form.valid) {

      this.service.tags = this.tags;

      console.log('onSave ', this.service);

      if (this.isNew) {
        const sub = this.jobServicesService.create(this.service)
          .subscribe(
            () => {
              this.toastrService.success('Din tjänst har skapats!');
              this.userService.reloadOrganisation();
              this.router.navigate([this.callback]);
            },
            (error) => {
              this.toastrService.error('Misslyckades att registrera tjänsten på servern.');
              console.error('Failed to register the service', error);
            },
            () => sub.unsubscribe());
      } else {
        console.log(this.service);
        const sub = this.jobServicesService.update(this.service)
          .subscribe(
            () => {
              this.toastrService.success('Ändringarna har sparats!');
              this.userService.reloadOrganisation();
              this.router.navigate([this.callback]);
            },
            (error) => {
              this.toastrService.error('Misslyckades att spara ändringarna på servern');
              console.error('Failed to save the changes', error);
            },
            () => sub.unsubscribe());
      }
    } else {
      this.toastrService.error('Tjänsten är inte korrekt ifylld, korrigera formuläret för att kunna spara.');
      return false;
    }
  }

  onReset() {
    this.service = Object.create(this.originalService);
    this.tags = this.originalService.tags;
  }

  onBackToList() {
    this.router.navigate([this.callback]);
  }

  public convertToLower(tag: String): Observable<String> {
    return of(tag.toLowerCase());
  }

  changeCategory(object) {
    this.service.categories = this.service.categories.filter(category => category.identifier !== object.identifier);
    if (object.event.currentTarget.checked) {
      this.service.categories.push(
        {identifier: object.identifier, name: object.name, description: null, targetEmployer: null, targetWorker: null, subs: null});
    }
  }

  public resetCategories() {
    this.service.categories = [];
    this.categoryComp.selectedCategories = {};
  }

  changeIndustry(identifier: string, event) {
    this.service.industries = this.service.industries.filter(industry => industry.identifier !== identifier);
    if (event.currentTarget.checked) {
      this.service.industries.push(new Industry(identifier));
    }
  }
}
