import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {User} from '../../../../shared/model/user';
import {OrganisationService} from '../../../../shared/services/organisation.service';
import {KeycloakService} from '../../../../shared/services/keycloak/keycloak.service';
import {ToastrService} from 'ngx-toastr';
import {Organisation} from '../../../../shared/model/organisation';

@Component({
  selector: 'app-org-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnChanges {

  public invitesToSend = '';

  constructor(private orgService: OrganisationService,
              public keycloakService: KeycloakService,
              private toastrService: ToastrService) {
  }

  @Input()
  public organisation: Organisation;

  @Output()
  update = new EventEmitter<Organisation>();

  ngOnChanges() {

  }

  public isAcceptedMember(): boolean {
    const usersKId = this.keycloakService.client().subject;
    const members = this.organisation.members.filter(user => user.keycloakId === usersKId);

    if (members.length > 0) {
      return members[0].accepted;
    }
  }

  public sendInvites() {
    console.log('sendInvites', this.invitesToSend);
    this.orgService.sendInvite(this.organisation, [this.invitesToSend])
      .subscribe(() => {
        this.toastrService.success('Inbjudan skickades till ' + this.invitesToSend);
        this.invitesToSend = '';
      }, error => {
        this.toastrService.error('Det gick inte att skicka inbjudan, ' + error);
        console.log('Det gick inte att skicka inbjudan, ', error);
      });
  }

  public acceptMemberRequest(member: User) {

    const subscription = this.orgService.acceptOrRejectOrganisationMember(this.organisation, member, true).subscribe(
      savedOrg => {
        console.log('acceptMemberRequest: ', savedOrg);
        this.update.emit(savedOrg);
      }, error => {
        console.log('Failed to accept member: ', error);
        this.toastrService.error('Det gick inte att acceptera medlemmen');
        subscription.unsubscribe();
      }
      , () => subscription.unsubscribe()
    );
  }

  public rejectMemberRequest(member: User) {
    const subscription = this.orgService.acceptOrRejectOrganisationMember(this.organisation, member, false).subscribe(
      savedOrg => {
        console.log('rejectMemberRequest: ', savedOrg);
        if (member.keycloakId === this.keycloakService.client().subject) {
          savedOrg = null;
        }

        this.update.emit(savedOrg);
      }, error => {
        console.log('Failed to reject member: ', error);
        this.toastrService.error('Det gick inte att neka medlemmen');
        subscription.unsubscribe();
      }
      , () => subscription.unsubscribe()
    );
  }

  public removeMember(member: User) {
    const accept = confirm(`Vill du ta bort ${member.name} ur organisationen`);
    if (accept) {
      const subscription = this.orgService.removeOrganisationMember(this.organisation, member).subscribe(
        savedOrg => {
          console.log('removeMember: ', savedOrg);
          if (member.keycloakId === this.keycloakService.client().subject) {
            savedOrg = null;
          }
          this.update.emit(savedOrg);
        }, error => {
          console.log('Failed to remove member: ', error);
          this.toastrService.error('Det gick inte att ta bort medlemmen');
          subscription.unsubscribe();
        }
        , () => subscription.unsubscribe()
      );
    }
  }
}
