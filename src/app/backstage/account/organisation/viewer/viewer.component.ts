import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {Organisation} from '../../../../shared/model/organisation';
import {OrganisationService} from '../../../../shared/services/organisation.service';
import {ToastrService} from 'ngx-toastr';
import {KeycloakService} from '../../../../shared/services/keycloak/keycloak.service';
import {User} from '../../../../shared/model/user';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {MetadataService} from '../../../../shared/services/metadata.service';

@Component({
  selector: 'app-org-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnChanges {

  @Input()
  public organisation: Organisation;
  @Output() update = new EventEmitter<Organisation>();
  @Output() edit = new EventEmitter();

  public imageUrl: Observable<string>;
  constructor(private metadataService: MetadataService,) {
  }

  ngOnChanges() {
    this.imageUrl = this.metadataService.metadata
      .pipe(
        map(meta => {
          // console.log('Got a metadata object ', meta, this.image);
          if (this.organisation.logoUrl && this.organisation.logoUrl.startsWith('/')) {
            return meta.imageStoreUrl + this.organisation.logoUrl;
          }
          return this.organisation.logoUrl;
        }));
  }

  public enterEditMode() {
    this.edit.emit();
  }

}
