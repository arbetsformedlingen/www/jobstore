import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Organisation} from '../../../../shared/model/organisation';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrganisationService} from '../../../../shared/services/organisation.service';
import {ToastrService} from 'ngx-toastr';
import {KeycloakService} from '../../../../shared/services/keycloak/keycloak.service';

import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-org-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnChanges {

  @Input()  public organisation: Organisation;
  @Output() public updated = new EventEmitter<Organisation>();
  @Output() public cancel = new EventEmitter();

  public edit: Organisation = new Organisation();

  constructor(private orgService: OrganisationService,
              public keycloakService: KeycloakService,
              private modalService: NgbModal,
              private toastrService: ToastrService) {
  }


  ngOnChanges() {
    this.edit = this.organisation;
  }

  newImageUploade(image: File) {
    console.log('new image to upload ', image);
    this.orgService.addOrganisationImage(this.organisation, image)
      .subscribe(res => {
        console.log('SUCCESS', res);
        this.toastrService.success('Bilden laddades upp för din organisation!');
        this.organisation.logoUrl = `${res['link']}?nocache=${Math.ceil(Math.random() * 999999)}`;
        console.log('this.organisation.image=', this.organisation.logoUrl);
      }, error => {
        console.log('FAILED TO UPLOAD: ', error);
        this.toastrService.error('Misslyckades att ladda upp din bild till servern, försök igen senare');
      });
  }

  imageDeleted(imageName: string) {
    console.log('deleting organisation image');
    this.orgService.removeImage(this.organisation)
      .subscribe(res => {
        this.organisation.logoUrl = '';
      }, error => {
        this.toastrService.error('Det gick inte att ta bort bilden');
      });
  }

  public doCancel() {
    this.cancel.emit();
  }

  public doDelete() {
    const confirmed = confirm('Vill du ta bort organisationen och alla tjänster?');
    if (confirmed) {
      this.delete();
    }
  }

  public delete() {
    const subscription = this.orgService.delete(this.organisation).subscribe(
      respond => {
        this.toastrService.info(`Organisationen ${this.organisation.name} är borttagen`);
        this.updated.emit(null);
      },
      error => {
        console.log('Failed to delete the Organisation: ', error);
        this.toastrService.error('Det gick inte att ta bort organisationen');
        subscription.unsubscribe();
      }, () => subscription.unsubscribe()
    );
  }

  public save() {
    console.log('Organisation save: ', this.edit);
    const subscription = this.orgService.update(this.edit).subscribe(
      saved => {
        console.log('Organisation was saved: ', saved);
        this.updated.emit(saved);
      },
      error => {
        console.log('Organisation failed to be saved: ', error);
        this.toastrService.error('Det gick inte att spara organisationen');
        subscription.unsubscribe();
      }
      , () => subscription.unsubscribe()
    );
  }
}
