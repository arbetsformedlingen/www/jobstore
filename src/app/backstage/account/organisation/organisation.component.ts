import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {Organisation} from '../../../shared/model/organisation';
import {KeycloakService} from '../../../shared/services/keycloak/keycloak.service';

@Component({
  selector: 'app-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['./organisation.component.scss'],

})

export class OrganisationComponent implements OnChanges {

  @Input() public organisation: Organisation;
  @Output() updated = new EventEmitter<Organisation>();

  public editMode = false;

  constructor(public keycloakService: KeycloakService) {
  }

  ngOnChanges() {
    this.exitEditMode();
  }

  public fireUpdateEvent(updated: Organisation | null) {
    this.updated.emit(updated);
  }

  protected enterEditMode() {
    this.editMode = true;
  }

  protected exitEditMode() {
    this.editMode = false;
  }
}
