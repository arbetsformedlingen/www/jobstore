import {Component, OnInit, OnDestroy, OnChanges, Input} from '@angular/core';
import {JobServiceRegistrationService} from '../../../shared/services/job-service-registration.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Organisation} from '../../../shared/model/organisation';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {ServiceSummary} from '../../../shared/model/service-summary';

@Component({
  selector: 'app-job-services-list',
  templateUrl: './job-service-list.component.html',
  styleUrls: ['./job-service-list.component.scss']
})
export class JobServiceListComponent implements OnInit, OnChanges {

  @Input() organisation: Organisation;

  constructor(private router: Router,
              private toastrService: ToastrService,
              protected jobServicesService: JobServiceRegistrationService,
              private orgService: OrganisationService) {
  }

  ngOnInit() {
    // console.log('ngOnInit ', this.organisation);
  }

  ngOnChanges() {
    // console.log('ngOnChanges ', this.organisation);
    if (this.organisation) {
    }
  }
  onAddNew() {
    this.router.navigate(['/backstage/service/add']);
  }

  onEdit(identifier) {
    this.router.navigate(['/backstage/service', identifier]);
  }

  doEncodeURI(url) {
    return encodeURI(url);
  }

  togglePublish(registration: ServiceSummary) {
    if (registration.published) {
      const sub = this.jobServicesService.unpublish(registration)
        .subscribe(
          () => { this.toastrService.info(`Tjänsten ${registration.name} är nu avpublicerad`); },
          error => {
            this.toastrService.error('Failed to unpublish registration');
            console.error('Failed to unpublish registration', error);
          },
          () => sub.unsubscribe());
    } else {
      const sub = this.jobServicesService.publish(registration)
        .subscribe(
          () => { this.toastrService.info(`Tjänsten ${registration.name} är nu publicerad`); },
          error => {
            this.toastrService.error('Failed to publish registration');
            console.error('Failed to publish registration', error);
          },
          () => sub.unsubscribe());
    }
  }
}
