import { Component, OnInit } from '@angular/core';
import {KeycloakService} from '../../../shared/services/keycloak/keycloak.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {OrganisationService} from '../../../shared/services/organisation.service';
import {Observable} from 'rxjs';
import {Organisation} from '../../../shared/model/organisation';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-backstage-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public isNavbarCollapsed = false;
  public organisation: Observable<Organisation>;

  constructor(private router: Router,
              public keycloakService: KeycloakService,
              private userService: UserService,
              private location: Location) { }

  static getHostUrl() {
    return window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
  }

  ngOnInit() {
    this.organisation = this.userService.userOrganisation;
  }

  doLogout() {
    console.log('doLogout');
    this.keycloakService.client().logout({redirectUri: NavbarComponent.getHostUrl() + '/backstage'});
  }

  doLogin() {
    console.log('doLogin');
    this.keycloakService.client().login()
      .success(result => console.log('Logged in succesful', result))
      .error(error => console.log('Login failed ', error));
  }

  doRegister() {
    console.log('doRegister');
    this.keycloakService.client().register();
  }

  goToSearch() {
    this.router.navigate(['/search']);
  }

  toggelNavbar() {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

}
