import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BackstageRoutingModule} from './backstage-routing.module';
import {HomeComponent} from './home/home.component';
import {NavbarComponent} from './layouts/navbar/navbar.component';
import {BackstageComponent} from './backstage.component';
import {FooterComponent} from './layouts/footer/footer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {JobServiceListComponent} from './account/job-service-list/job-service-list.component';
import {JobServiceEditorComponent} from './account/job-service-editor/job-service-editor.component';
import {ProfileComponent} from './account/profile/profile.component';
import {ToastrModule} from 'ngx-toastr';
import {TagInputModule} from 'ngx-chips';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; // this is needed!
import {OrganisationComponent} from './account/organisation/organisation.component';
import {KeycloakGuard} from '../shared/services/keycloak/keycloak.guard';
import {OntologyService} from '../shared/services/ontology.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EditorComponent } from './account/organisation/editor/editor.component';
import { ViewerComponent } from './account/organisation/viewer/viewer.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImagePickerComponent } from './image-picker/image-picker.component';
import { MembersComponent } from './account/organisation/members/members.component';
import { InvitesComponent } from './invites/invites.component';
import { AddOrganisationComponent } from './onboarding/add-organisation/add-organisation.component';
import { CodeOfConductComponent } from './code-of-conduct/code-of-conduct.component';
import { InfoBoxComponent } from './info-box/info-box.component';
import { WizardComponent } from './onboarding/wizard/wizard.component';
import { AddServiceComponent } from './onboarding/add-service/add-service.component';
import { UniqueOrganisationNameValidatorDirective } from './onboarding/add-organisation/unique-organisation-name-validator.directive';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CategoriesComponent } from './categories/categories.component';
import {Angulartics2Module} from 'angulartics2';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BackstageRoutingModule,
    ToastrModule.forRoot({positionClass: 'toast-top-full-width'}),
    TagInputModule,
    NgbModule,
    ImageCropperModule,
    NgxSpinnerModule,
    Angulartics2Module
  ],
  declarations: [
    HomeComponent,
    NavbarComponent,
    BackstageComponent,
    FooterComponent,
    ProfileComponent,
    JobServiceListComponent,
    JobServiceEditorComponent,
    OrganisationComponent,
    EditorComponent,
    ViewerComponent,
    ImagePickerComponent,
    ViewerComponent,
    MembersComponent,
    InvitesComponent,
    AddOrganisationComponent,
    CodeOfConductComponent,
    InfoBoxComponent,
    WizardComponent,
    AddServiceComponent,
    UniqueOrganisationNameValidatorDirective,
    CategoriesComponent
  ],
  providers: [
    KeycloakGuard,
    OntologyService,
  ],
})
export class BackstageModule { }
