import { Component, OnInit, OnChanges, Input, Output, EventEmitter} from '@angular/core';
import { CategoryService } from '../../shared/services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit, OnChanges {

  @Input() public theForm;
  @Input() public service;
  @Input() public targetGroup;

  @Output() public changedCategory = new EventEmitter<object>();

  // public selectedCategories = JSON.parse('{"jobsearch": true}');
  public selectedCategories = {};
  public lastTargetGroup: string;
  private categoriesCopy: any[];
  public selectedCopy = {};

  constructor(
    public categoryService: CategoryService) { }

  ngOnInit() {
    this.service.categories.forEach(cat => this.selectedCategories[cat.identifier] = true);

    this.lastTargetGroup = this.targetGroup;
    this.categoryService.asynCategories.subscribe(result => { this.categoriesCopy = result;
    if (Object.keys(this.selectedCategories).length > 0) {
        this.initSelectedCopy(result);
      }
    });
  }

  ngOnChanges() {
    if (this.lastTargetGroup !== this.targetGroup) {
      this.lastTargetGroup = this.targetGroup;
    }
  }

  public changeCategory(identifier, event, name) {
    if (this.selectedCategories[identifier] === true) {
      this.addParents(identifier, this.categoriesCopy, event);
    } else if (this.selectedCategories[identifier] === false) {
      this.killChildren(identifier, this.selectedCopy, event);
    }
    this.changedCategory.emit({identifier, event, name});
  }

  public filterCats(option) {
    if ((this.lastTargetGroup === 'WORKER') && option.targetWorker) {
      return true;
    } else if ((this.lastTargetGroup === 'EMPLOYER') && option.targetEmployer) {
      return true;
    } else { return false; }
  }

  // GOOD LUCK.
  private addParents(id, cats, event) {
    for ( const cat of cats ) {
      if (cat['identifier'] == id) {
        this.selectedCopy[id] = cat;
        return true;
      } else if (cat['subs'].length > 0) {
        if ((this.addParents(id, cat['subs'], event))) {
          const identifier = cat.identifier;
          const name = cat.name;
          this.selectedCopy[identifier] = cat;
          this.selectedCategories[identifier] = true;
          this.changedCategory.emit({identifier, event, name});
          return true;
        }
      }
    }
  }

  // TODO: REWRITE THIS TO A RECURSIVE FUNCTION FOR MORE FLEXIBILITY
  private killChildren(id, selectedSubarray, event) {
    if (selectedSubarray[id].subs.length > 0) {
      for ( const sub of selectedSubarray[id].subs ) {
        if (sub.identifier in this.selectedCategories && this.selectedCategories[sub.identifier] === true) {
          const identifier = sub.identifier;
          const name = sub.name;
          this.selectedCategories[sub.identifier] = false;
          this.changedCategory.emit({identifier, event, name});

          if (sub.subs.length > 0) {
            for (const s of sub.subs) {
              if (s.identifier in this.selectedCategories && this.selectedCategories[s.identifier] === true) {
                const ids = s.identifier;
                const namey = s.name;
                this.selectedCategories[s.identifier] = false;
                this.changedCategory.emit({identifier: ids, event, name: namey});
              }
            }
          }
        }
      }
    }
  }

  // TODO: REWRITE THIS TO A RECURSIVE FUNCTION FOR MORE FLEXIBILITY
  private initSelectedCopy(result) {
    for (const x of result) {
      if (x.identifier in this.selectedCategories) {
        this.selectedCopy[x.identifier] = x;
        for (const y of x.subs) {
          if (y.identifier in this.selectedCategories) {
            this.selectedCopy[y.identifier] = y;
            for (const z of y.subs) {
              if (z.identifier in this.selectedCategories){
                this.selectedCopy[z.identifier] = z;
              }
            }
          }
        }
      }
    }
  }

}
