import { Component, EventEmitter, Input, OnChanges, Output, ViewChild, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MetadataService } from '../../shared/services/metadata.service';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss']
})
export class ImagePickerComponent implements OnChanges, OnInit {

  @Input() public image: string;
  @Output() public uploaded = new EventEmitter<File>();
  @Output() public deleted = new EventEmitter<string>();

  @ViewChild('edit') input;

  public croppedImage: any = '';  // THE BASE 64 STRING FOR ADD AND UPDATE API CALL
  public cropppedImageFile: File;  // THE CROPPED IMAGE AS PNG FILE
  public imageChangedEvent: any = '';
  public imageBase64: any = '';
  public imgUrl: string;
  public uploadingImage = false;
  public editTemplate: any;

  public imageUrl: Observable<string>;

  private dialogRef: NgbModalRef;

  constructor(private modalService: NgbModal,
              private toastrService: ToastrService,
              private metadataService: MetadataService,
              private http: HttpClient,
              private spinner: NgxSpinnerService) { }


  ngOnInit(): void {
    this.spinner.hide();
  }

  ngOnChanges() {
    this.croppedImage = '';
    this.cropppedImageFile = null;
    this.imageUrl = this.metadataService.metadata
      .pipe(
        map(meta => {
          // console.log('Got a metadata object ', meta, this.image);
          if (this.image && this.image.startsWith('/')) {
            return meta.imageStoreUrl + this.image;
          }
          return this.image;
        }));
  }

  public triggerModal(scrappedImg?: string): void {
    this.imgUrl = scrappedImg;
    this.dialogRef = this.modalService.open(this.input, { centered: true, backdrop : 'static' });
    this.getImageFromUrl(this.imgUrl);
  }

  getImageFromUrl(url: string): void {
    console.log(url);
    this.spinner.show();
    const encodedUrl = encodeURIComponent(url);
    const image = this.http.get(`${environment.serviceProviderUrl}/v1/jobstore/imageredirect?urlToImage=${encodedUrl}&asBase64=true`,
    {responseType: 'text'});
    image.subscribe(result => { this.imageBase64 = result; this.spinner.hide();},
      error => (console.log(error), this.spinner.hide()));
  }

  imageCropped(image: string) {
    this.croppedImage = image;
  }

  imageCroppedFile(image: File) {
    this.cropppedImageFile = image;
  }

  changeImage(): void {
    this.image = '';
  }

  fileChangeEvent(event: any): void {
    this.imgUrl = '';
    this.imageChangedEvent = event;
  }

  deleteImage(): void {
    console.log('delete Image ', this.image);
    this.deleted.emit(this.image);
    this.image = null;
    this.imgUrl = '';
    this.imageBase64 = '';
    this.croppedImage = '';
    this.cropppedImageFile = null;
    this.imageChangedEvent = null;
  }

  uploadImage(): void {
    console.log('upload Image ', this.cropppedImageFile);
    this.closeEditorDialog('Image saved')
    this.uploaded.emit(this.cropppedImageFile);
  }

  imageLoaded(): void {
    // show cropper
  }

  loadImageFailed(): void {
    this.toastrService.error('Misslyckades att ladda upp din bild, kontrollera att filformatet är i JPEG eller PNG');
  }

  imgUrlChange(newUrl: string) {
    this.imgUrl = newUrl;
  }

  openEditorDialog(content) {
    this.dialogRef = this.modalService.open(content, { windowClass : 'biggerModal', centered: true, backdrop : 'static'});
    console.log(this.spinner);
  }

  closeEditorDialog(result) {
    this.dialogRef.close(result);
  }
}
