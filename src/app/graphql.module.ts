import {NgModule} from '@angular/core';
import {ApolloModule, APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {defaultDataIdFromObject, InMemoryCache} from 'apollo-cache-inmemory';
import {environment} from '../environments/environment';

const uri = `${environment.serviceProviderUrl}/graphql`;
export function createApollo(httpLink: HttpLink) {
  return {
    link: httpLink.create({uri}),
    cache: new InMemoryCache()
    //   {
    //     dataIdFromObject: result => {
    //       console.log('dataIdFromObject for ', result.__typename);
    //       switch (result.__typename) {
    //         case 'SearchResult':
    //           return `${result.__typename}:${Math.random()}`;
    //         case 'SearchItem':
    //           return result.__typename + ':' + result.identifer; // use `bar` prefix and `blah` as the primary key
    //         default:
    //           return defaultDataIdFromObject(result); // fall back to default handling
    //       }
    //     }
    //   }
    // )
  // {
    //   dataIdFromObject: result => {
    //     // FIXME: workaround buggy apollo cache, dont cache certain types at all!
    //     switch (result.__typename) {
    //       case 'SearchResult':
    //         return `${result.__typename}:${Math.random()}`;
    //       default:
    //         return defaultDataIdFromObject(result); // fall back to default handling
    //     }
    //   }
    // }),
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
})
export class GraphQLModule {}
