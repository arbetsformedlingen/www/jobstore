
# Technology

* Angular 7.0.2
* Angular-Cli (at least 7.0.4)
* Bootstrap 4
* NodeJS 10
* NPM (at least 5.6.0)
* TS 3.1.6


# Development Environment

Once you have the code, you should execute:

> npm install

# Project Installation

This project was created using [angular-cli](./docs/angular-cli.md).  In addition, the following modules were installed:
  
* npm install --save bootstrap@4.0.0-alpha.6
* npm install --save @ng-bootstrap/ng-bootstrap
